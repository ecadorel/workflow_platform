import os
import zipfile
import subprocess
from os import walk
from pymongo import MongoClient
import pymongo
import requests
import json
import matplotlib.pyplot as plt
import numpy as np
import yaml
import math
import urllib3
urllib3.disable_warnings()
import argparse
import os
import pandas


global_report = {}

# =============================================================================================================
# ===                                           CHARGEMENT                                                 ====
# =============================================================================================================

# Recuperation des fichiers zip du dossier courant
# Chaque fichier est charge en base
def getAllZips () : 
    # f = []
    # for (dirpath, dirnames, filenames) in walk("."):
    #     for j in filenames :
    #         if (len (j) >= 4 and j [-4:] == ".zip") :
    #             f = f + [j]
    #     break
    # return f
    parser = argparse.ArgumentParser()

    parser.add_argument("elem", help="input file", nargs="*")
    args = parser.parse_args ()
    return args.elem


def loadInDatabase (path) :
    with zipfile.ZipFile(path, 'r') as zip_ref:
        zip_ref.extractall(".")

    client = MongoClient ()
    client.drop_database ("tmp_base")
    cmd = "mongorestore backup/orch --drop -d tmp_base"
    print subprocess.check_output(cmd,stderr=subprocess.STDOUT,shell=True)

# =============================================================================================================
# ===                                           GRAPHIQUES                                                 ====
# =============================================================================================================
    
# Le premier graphe est celui de l'energie
# Le deuxieme celui du taux d'utilisation cpu
# Le troisieme celui de l'arrive des utilisateurs
def generateGraphs (name) : 
    generateEnergy (name)
    generateCpuUsage (name)
    generateUserLoad (name)

def generateEnergy (name) :
    client = MongoClient ()
    db = client.tmp_base
    min_time = db.sched_info.find_one (sort=[("time", -1)])["time"] # l'instant ou le sched se connecte a l'orch
    max_task = db.tasks.find_one (sort=[("end", -1)])
    mongo_nodes = db.nodes.find ()
    nodes = []
    for n in mongo_nodes : #on est oblige de copier, si on ferme la base la liste est invalide
        nodes = nodes + [n]

    makespan = max_task["end"]/1000 - min_time/1000 # temps d'exec totale
    print (makespan)
    
    # Recuperation des sensors sur l'api seduce
    req_sensors = requests.get ("https://api.seduce.fr/sensors", verify=False) 
    json_sensors = json.loads (req_sensors.text)
    used_sensors = []
    for sens in json_sensors ["sensors"] : 
        for node in nodes:
            if (len (sens) >= len (node["n_id"]) + 1):
                if (sens [0:len (node["n_id"]) + 1] == node["n_id"]+"_") :
                    used_sensors = used_sensors + [sens[:]]

    print (used_sensors)
    conso_result = {}
    conso_global = []


    # Recuperation des valeurs d'energie
    max_time = max_task ["end"]
    IDLE = 75
    MAX_CONSO = 145
    for sensor in used_sensors : 
        req_result = requests.get("https://api.seduce.fr/sensors/" + sensor + "/measurements?start_date=" + str (int(min_time/1000)) + "&end_date=" + str (int(max_time/1000)), verify=False)
        json_result = json.loads (req_result.text)
        node_name = sensor [0:sensor.find ("_")]
        if node_name in conso_result : 
            conso_result [node_name] = [x + y for x, y in zip (conso_result[node_name], json_result ["values"])]
        else : 
            conso_result [node_name] = json_result ["values"]
            
        if (conso_global == []): 
            conso_global = json_result ["values"]
        else :
            conso_global = [x + y for x, y in zip (json_result ["values"], conso_global)]


    MAX_MAKESPAN = 520
    idle_conso = (MAX_MAKESPAN - makespan) * len (nodes) * IDLE
    max_conso = MAX_MAKESPAN * len (nodes) * MAX_CONSO
    
    if ("conso_global" in global_report) : 
        global_report ["conso_global"][name] = sum (conso_global)
        global_report ["conso_widle"][name]  = sum (conso_global) + idle_conso
        global_report ["max_conso"][name] = max_conso
        global_report ["percent_conso"][name] = global_report["conso_widle"][name] / (max_conso*1.0)
        global_report ["idle"][name]  = idle_conso
        global_report ["makespan"][name] = makespan
    else :
        global_report ["conso_global"] = {name : sum(conso_global)}
        global_report ["conso_widle"]  = {name : sum(conso_global) + idle_conso}
        global_report ["max_conso"] = {name : max_conso}
        global_report ["percent_conso"] = {name : global_report["conso_widle"][name] / (max_conso*1.0)}
        global_report ["idle"]  = {name : idle_conso}
        global_report ["makespan"] = {name : makespan}

    # Generation du graphe
    # os.makedirs ("energy", exist_ok=True)
    plt.figure (1)
    plt.plot (conso_global, linewidth=0.5, label=name[:-4])

def generateCpuUsage (name) :
    client = MongoClient ()
    db = client.tmp_base
    max_task = db.tasks.find_one (sort=[("end", -1)])
    min_task = db.tasks.find_one (sort=[("start", 1)])
    tasks = db.tasks.find ()
    
    makespan = max_task["end"]/1000 - min_task["start"]/1000
    load = [0 for i in xrange (0, makespan)]
    for x in tasks :
        for j in xrange (x["start"]/1000, x["end"]/1000) :
            i = j - min_task ["start"]/1000
            load [i] = load [i] + 1
    plt.figure (2)
    plt.plot (load, linewidth=0.5, label=name[:-4])

def generateUserLoad (name) :
    client = MongoClient ()
    db = client.tmp_base
    min_time = db.sched_info.find_one (sort=[("time", -1)])["time"] # l'instant ou le sched se connecte a l'orch
    max_task = db.tasks.find_one (sort=[("end", -1)])
    
    makespan = max_task["end"]/1000 - min_time/1000 # temps d'exec totale
    print (makespan)
    if (makespan > 0) : 
        load = [0 for i in xrange (0, makespan)]
        with open ("backup/workload.yaml", 'rb') as stream : 
            try : 
                load2 = yaml.load (stream) 
                for flow in load2 ['workload'] :
                    load [int (flow ['start'])] = load [int (flow['start'])] + 1
            except yaml.YAMLError as exc:
                print(exc)        
        plt.figure (3)
        plt.plot (load, label=name[:-4])



        
# =============================================================================================================
# ===                                             ANALYSE                                                  ====
# =============================================================================================================

# Analyse de la conso est faite au moment de la generation du graphique
# Analyse du nombre d'utilisateur
# Analyse de la diff entre utilisateur et makespan
def analyse (name) :
    client = MongoClient ()
    db = client.tmp_base
    users = db.tasks.distinct ("user")
    vms = db.vms.find ()
    min_time = db.sched_info.find_one (sort=[("time", -1)])["time"] # l'instant ou le sched se connecte a l'orch
    deadlines = {}
    with open ("backup/workload.yaml", 'rb') as stream : 
        try : 
            load = yaml.load (stream) 
            for flow in load ['workload'] : 
                deadlines [flow ['user']] = int (flow ['start']) + int(flow ['deadline'])
        except yaml.YAMLError as exc:
            print(exc)        

    print (deadlines)
    max_end_users = {}
    nb_content = 0
    nb_pas_content = 0
    diff_content = 0
    diff_pas_content = 0
    max_pas_content = 0
    min_pas_content = 90000000000
    pas_content = []
    pas_content_list = []
    
    for user in users :
        max_task = db.tasks.find ({"user" : user}).sort("end", pymongo.DESCENDING)[0]
        max_end_users[user] = int ((max_task ["end"] - min_time)/1000)
        if (deadlines [user] >= max_end_users [user]) :
            nb_content = nb_content + 1
            diff_content = diff_content + (deadlines [user] - max_end_users [user])
            pas_content_list = pas_content_list + [max_end_users [user] - deadlines[user]]
        else :
            nb_pas_content = nb_pas_content + 1
            diff_pas_content = diff_pas_content + (deadlines [user] - max_end_users [user])
            pas_content = pas_content + [user]
            pas_content_list = pas_content_list + [max_end_users [user] - deadlines[user]]
            print (deadlines [user] - max_end_users[user])
            if (abs (deadlines [user] - max_end_users[user]) > max_pas_content) :
                max_pas_content = abs (deadlines [user] - max_end_users[user])
            if (abs (deadlines [user] - max_end_users[user]) < min_pas_content) :
                min_pas_content = abs (deadlines [user] - max_end_users[user])
                
    print (max_end_users)
    if ("diff_content" in global_report) : 
        global_report ["diff_content"][name] = diff_content
        global_report ["diff_pas_content"][name] = diff_pas_content
        global_report ["max_pas_content"][name] = max_pas_content
        global_report ["min_pas_content"][name] = min_pas_content
        global_report ["nb_content"][name] = nb_content
        if (nb_pas_content != 0) :
            global_report ["mean_pas_content"][name] = (diff_pas_content / (nb_pas_content * 1.0))
        else :
            global_report ["mean_pas_content"][name] = 0
        global_report ["nb_vms"][name] = vms.count ()
        df = pandas.DataFrame (np.array (pas_content_list))
        global_report ["quantile"][name] = pas_content_list
    else :
        global_report ["diff_content"] = {name : diff_content}
        global_report ["diff_pas_content"] = {name : diff_pas_content}
        global_report ["max_pas_content"] = {name : max_pas_content}
        global_report ["min_pas_content"] = {name : min_pas_content}
        if nb_pas_content != 0 : 
            global_report ["mean_pas_content"] = {name: diff_pas_content / (nb_pas_content * 1.0)}
        else :
            global_report ["mean_pas_content"] = {name: 0}
        global_report ["nb_content"] = {name : nb_content}
        global_report ["nb_vms"] = {name : vms.count ()}
        global_report ["quantile"] = {name: pas_content_list}



def exportGlobalReport () :
    lines = {}
    head = ""
    for k in sorted (global_report.keys ()) :
        values = global_report [k]
        head = head + k.center (20) + "|"
        for j in values :
            if (j in lines) :
                lines [j] = lines [j] + str (values [j]).center (20) + '|'
            else :
                lines [j] = str (values [j]).center (20) + '|'
    sret = ("").center (30) + '|' + ("").center (len (head), '#') + '\n' + ("").center (30) + '|' + head + "\n"
    for l in lines :        
        sret = sret + l.center (30) + '|' + lines [l] + '\n'
    return sret
    

def generateTimes (zip) :
    client = MongoClient ()
    db = client.tmp_base
    times = {}
    variance = {}
    nb = {}
    for t in db.tasks.find () :
        if t["user"] == "c_0" : 
            if t["e_id"] in times :
                times [t["e_id"]] = times [t["e_id"]] + (t["end"] - t["start"])            
                nb [t["e_id"]] = nb [t["e_id"]] + 1
            else :
                nb [t["e_id"]] = 1
                times [t["e_id"]] = (t["end"] - t["start"])
                variance [t["e_id"]] = 0

    for t in db.tasks.find ():
        if (t ["user"] == "c_0"): 
            time = t["end"] - t["start"]
            variance [t["e_id"]] = variance [t["e_id"]] + ((time - (times [t["e_id"]]/nb[t["e_id"]])) * (time - (times [t["e_id"]]/nb[t["e_id"]])))


        
    for x in times :
        variance [x] = math.sqrt (variance[x]/nb[x])/1000.
        times[x] = times [x]/nb[x]/1000.

    times ["ubuntu"] = 0
    nb ["ubuntu"] = 0
    for v in db.vms.find () :
        time = (v["ready"] - v["start"])/1000
        times ["ubuntu"] = times ["ubuntu"] + time
        nb["ubuntu"] = nb["ubuntu"] + 1
        print (str (v["ready"]) + " " + str(v["start"]) + " " + str ((v["ready"] - v["start"])/1000))

    times ["ubuntu"] = times ["ubuntu"]/nb["ubuntu"]
    print (str (nb["ubuntu"]) + " " + str (times ["ubuntu"]))
    print (times)
    print (variance)
    print (nb)
                
def main () :
    plt.figure (1)
    plt.clf ()
    plt.figure (2)
    plt.clf ()
    plt.figure (3)
    plt.clf ()
    plt.figure (4)
    plt.clf ()
    zips = getAllZips ()
    for zip in zips :
        print ("Generate : " + zip)
        loadInDatabase (zip)
        generateTimes (zip)
        generateGraphs (zip)            #
        analyse (zip)

    plt.figure (1)
    plt.legend ()
    plt.savefig ("energy.png")    
    plt.figure (2)
    plt.legend ()
    plt.savefig ("load.png")

    plt.figure (3)
    plt.legend ()
    plt.savefig ("users.png")
    print (exportGlobalReport ())
    
if __name__ == "__main__" :
    main ()
