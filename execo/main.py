
from execo import *
from execo_g5k import *
import yaml
import datetime
from shutil import copyfile 
import ntpath
import yaml
import os
import sys
import random
import argparse
known_roots = {"ecotype" : "172.16.193.", "econome" : "172.16.192."}
known_caps = {"ecotype" : {"cpus" : 20, "memory" : 32768}, "econome" : {"cpus" : 16, "memory" : 16000}}
known_speed = {"ecotype" : 1800, "econome" : 2200}
known_bw = {"ecotype": 10000, "econome" : 10000}
conn_params = {'user': 'root'}
orch = []
daemons = []

def getAddrOfNode (node) :
    cluster = ""
    addr = node.address
    cluster = addr.split (".")[0].split ("-")[0]
    res = known_roots [cluster] + (addr.split (".")[0].split ("-")[1])
    return (res, cluster)

def createConfigFile (daemons) :
    computes= []
    for n in daemons :
        (addr, cluster) = getAddrOfNode (n)
        computes += [{
            "addr" : addr,
            "port" : 5200,
            "name" :  n.address.split (".")[0],
            "cluster" : cluster,
            "speed" : known_speed [cluster],
            "capacities" : known_caps [cluster]
        }]

    clusters= []
    input_bw = {'input' : 10000, 'output' : 10000}
    output_bw = {'input' : 10000, 'output' : 10000}
    for c in known_bw :
        clusters += [{
            "name" : c,
            "bw" : {c : known_bw[c], "input": 1, "output" : known_bw[c]}
        }]
        
        input_bw [c] = known_bw[c]
        output_bw [c] = known_bw[c]
        
    clusters += [{
        "name" : "input",
        "bw" : input_bw
    }, {
        "name" : "output",
        "bw" : output_bw
    }]

    with open ("config.yaml", "w") as stream :
        try :
            yaml.dump ({"computes" : computes, "cluster" : clusters}, stream, default_flow_style=True, allow_unicode=True)                    
        except yaml.YAMLError as exc:
            print(exc)    
    

def startUp (started) :
    global orch
    global daemons
    
    jobs = get_current_oar_jobs (["nantes"])
    logger.info ("Current job : " + str (jobs))

    running_jobs = [ job for job in jobs if get_oar_job_info(*job).get("state") == "Running" ]
    logger.info ("Current running job : " + str (jobs))
    
    nodes = sorted ([ job_nodes for job in running_jobs for job_nodes in get_oar_job_nodes (*job) ], key=lambda x: x.address)
    logger.info ("Current nodes : " + str (nodes))

    if len (nodes) < 1 :
        print ("Impossible")
        exit (0)

    orch = [nodes [0]]
    daemons = list (nodes [1:10])
    print (len (daemons))

    deployed, undeployed = deploy (Deployment (daemons, env_name="virt_1804"))
    deployed2, undeployed2 = deploy (Deployment (orch, env_name="ubuntu1804-x64-min"))
    logger.info ("Current deployed : " + str (deployed) + " undeployed : " + str (undeployed))


    if len (nodes) == 1 :
        daemons = daemons + [deployed[0]]
    
    mongo_start = Remote (
        "sudo apt update ; sudo apt install -y mongodb ; sudo service mongodb start ; sudo apt install -y openjdk-11-jre-headless unzip zip openjdk-11-jdk-headless ruby curl emacs",
        orch, conn_params
    )

    daemon_update = Remote (
        "sudo apt update ; sudo apt install -y libvirt-bin openjdk-11-jre-headless openjdk-11-jdk-headless ruby curl libguestfs-tools dnsmasq-utils",
        daemons, conn_params
    )

    createConfigFile (daemons)

    if (started == False) :
        orch_send_config = Put (orch, ["config.yaml"], ".", connection_params=conn_params)

        brodacast2 = Put (nodes, ["file_execs"], ".", connection_params=conn_params)
        brodacast3 = Put (daemons, ["images"], ".", connection_params=conn_params)
        brodacast4 = Put (nodes, ["daemon.jar"], ".", connection_params=conn_params)
        
        brodacast4.run ()
        logger.info ("Start send")
        
        orch_send_config.run ()
        brodacast2.run ()
        brodacast3.run ()
        logger.info ("send images")

        brodacast3.wait ()

        mongo_start.run ()
        daemon_update.run ()        
        brodacast2.wait ()
        brodacast4.wait ()
        mongo_start.wait ()
        daemon_update.wait ()


def launchJob (i, algo) :
    print ("Launch job : " + str (i) + " named : " + algo)
    
    (root_addr, cluster) = getAddrOfNode (orch[0])
    orch_send_data = Put (orch, ["apps"], ".", connection_params=conn_params)
    run_clean = Remote ("./file_execs/clean", orch + daemons, conn_params)
    brodacast = Put (orch, [algo], ".", connection_params=conn_params)
    run_master = Remote ("java -cp daemon.jar com.orch.master.Master --addr " + root_addr + " --config config.yaml | tee out.mast", orch, conn_params)

    run_daemon = []
    for d in daemons :
        (d_addr, c) = getAddrOfNode (d)
        run_daemon += [Remote ("java -cp daemon.jar com.orch.daemon.Daemon --addr " + d_addr + " | tee out.deam", [d], conn_params)]
    
    run_sched = Remote ("java -cp " + ntpath.basename (algo) + " com.orch.scheduler.ScheduleMain --addr " + root_addr + " | tee out.sched", orch, conn_params)
    
    orch_send_data.run ()
    brodacast.run ()
    orch_send_data.wait ()    

    
    brodacast.wait ()
    
    logger.info ("cleaning")
    run_clean.run ()
    run_clean.wait ()

    logger.info ("start run daemon")

    for r in run_daemon :
        r.start ()

    sleep (1)
    logger.info ("start run master")
    run_master.start ()
    logger.info ("Master and daemon started")
    sleep (10)
    
    logger.info ("start run sched")
    
    run_sched.run ()
    run_master.wait (timeout=1500)
    logger.info ("Summary : \n" + Report ([run_master]).to_string ())

    retreive_master_res = Remote ("mongodump --db orch --out backup && cp apps/workload.yaml backup && cp out.* backup/ && zip -r master.zip backup", orch, conn_params)
    copy_master_res = Get (orch, ["master.zip"], connection_params=conn_params)
    
    retreive_daemon_res = []

    retreive_master_res.run ()
    retreive_master_res.wait ()

    copy_master_res.run ()
    copy_master_res.wait ()

    copyfile ("master.zip", "result/" + ntpath.basename (algo) + "_" + str (i) + ".zip")


def getApp (apps, dice) :
    uni = random.uniform (1, sum (dice))
    curr = 0
    all = 0
    for x in dice :
        all = all + int (x)
        if uni < all :
            return apps [curr]
        else :
            curr = curr + 1
    return apps [-1]

def getDead (deads) :
    uni = random.uniform (0, len (deads))
    return deads [int(uni)]

def generateScenario (load) :
    nb_basic_load = int (load ["apps"]["nb_basic_load"])
    dead_basic_load = int(load ["apps"]["dead_basic_load"])
    dead_disturb_load = load ["apps"]["dead_disturb_load"]
    apps = load ["apps"]["apps"]
    dice = load ["apps"]["dice"]
    distrib = load ["apps"]["arrival"]

    result_load = []
    user_id = 0
    for i in range (0, nb_basic_load) :
        time = {}
        time ["user"] = "c_" + str (user_id)
        time ["name"] = getApp (apps, dice)
        time ["start"] = int (0)
        time ["deadline"] = int (dead_basic_load)
        result_load = result_load + [time]
        user_id = user_id + 1

    for i in range (0, len (distrib)) :
        time = {}
        time ["user"] = "c_" + str (user_id)
        time ["name"] = getApp (apps, dice)
        time ["start"] = int (distrib[i])
        time ["deadline"] = getDead (dead_disturb_load)
        result_load = result_load + [time]
        user_id = user_id + 1
    print (result_load)
    with open ("apps/workload.yaml", 'w') as stream :
        try :
            yaml.dump ({"workload" : result_load}, stream)
                
        except yaml.YAMLError as exc :
            print (exc)
            
def readScenario () :
    startUp (False)
    with open ("scenario.yaml", 'rb') as stream :
        try :
            load = yaml.load (stream)
            generateScenario (load)
            for z in load ["scheduler"] : 
                launchJob (i, z)                    
        except yaml.YAMLError as exc :
            print (exc)
            None
                
readScenario ()
