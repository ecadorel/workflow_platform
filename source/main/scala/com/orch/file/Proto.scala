package com.orch.file

import akka.io.{ IO, Tcp }
import akka.actor.{ Actor, ActorRef, Props }

object FileProto {
    case object Ack extends Tcp.Event
    case object End extends Tcp.Event

    case class FileSent    (id : Long)
    case class ExeSent     (id : Long, params : Object)
    case class ExeFailure  (id : Long, e_id : String, v_id : Long, params : String)
    case class RecvFile    (addr : String, port : Int, path: String)
    case class FileFailure (id : Long)
}


