package com.orch.daemon

import java.io.{File}
import scala.Array
import akka.actor.{Props, Actor, ActorSystem, ActorRef, ActorLogging , ActorSelection }
import com.typesafe.config.ConfigFactory

import akka.util.ByteString
import java.net.InetSocketAddress

class Daemon (DAddr : String, DPort : Int) extends Actor with ActorLogging {
    import com.orch.daemon.DaemonProto._
    import com.orch.file._
    import com.orch.db._
    import com.orch.utils._


    var orchDaemon : ActorSelection = null
    var masterAddr = ""
    //var coll = new Collection ("daemon")

    override def preStart () : Unit = {
        FileIO.startServer ()
        Vagrant.startNfsDir ()
        //future.start ()
    }

    def receive : Receive = {
        case DaemonProto.RegisterMaster (addr, port) =>
            println ("Register master node " + addr + " " + port)
            masterAddr = addr
            orchDaemon = context.actorSelection (s"akka.tcp://RemoteSystem@$addr:$port/user/master")

        case DaemonProto.RecvFile (id, addr, t_idSrc, t_idDst, user, in_file, out_file) =>
            //log.info  (s"Send file : ${id}")
            val srcPath = Path.build (Path.build (Path.build (Global.user_home, user), "" + t_idSrc), in_file)
            val dstPath = "file:" + user + ":" + t_idDst + ":" + out_file

            FileIO.recvFile (self, id, srcPath.file, dstPath, addr)

        case DaemonProto.RecvInput (id, addr, t_idDst, user, in_file, out_file) => 
            val dstPath = "file:" + user + ":" + t_idDst + ":" + out_file
            FileIO.recvFile (self, id, in_file, dstPath, addr)

        case DaemonProto.RecvExe (id, e_id, args, srcPath, dstPath, addr) =>
            FileIO.recvExe (orchDaemon, id, e_id, args, srcPath, dstPath, addr)

        // case DaemonProto.DownloadResult (id,  user, in_file, t_idSrc, path) =>
        //     //log.info (s"Download result : ${id}")
        //     val srcPath = Path.build (Path.build (Path.build (Global.user_home, user), "" + t_idSrc), in_file)
        //     val dstPath = "output:" + path
        //     FileIO.sendFile (self, orchDaemon, context, id, srcPath.file, dstPath, orchDaemon.anchorPath.address.host.get)

        case FileProto.FileSent (id) =>
            //log.info (s"File sent $id");
            orchDaemon ! DaemonProto.FileSent (id)

        case FileProto.FileFailure (id) =>
            orchDaemon ! DaemonProto.FileFailure (id)

        case DaemonProto.LaunchVM (id, os, capas, user, script) =>
            //log.info (s"Launch vm : $id, with $os of $capas for user $user with $script")
            Vagrant.launchVM (id, os, capas, user, script, self)

        case DaemonProto.KillVM (id) =>
            //log.info (s"Kill vm : $id")
            Vagrant.killVM (id, self)
        case DaemonProto.VmReady (id, log_) =>
            //log.info (s"VM $id is ready")
            orchDaemon ! DaemonProto.VmReady (id, log_)
        case DaemonProto.VmError (id, log_) =>
            //log.info (s"Vm $id is error")
            orchDaemon ! DaemonProto.VmError (id, log_)
        case DaemonProto.VmOff (id, log_) =>
            //log.info (s"Vm $id is down")
            orchDaemon ! DaemonProto.VmOff (id, log_)
        case DaemonProto.RunTask (id, user, v_id, params) =>
            //log.info (s"Run $id task")
            Vagrant.runTask (id, user, v_id, params, self)
        case DaemonProto.TaskEnd (id) =>
            //log.info (s"Task $id finished")
            orchDaemon ! DaemonProto.TaskEnd (id)
        case DaemonProto.TaskFailure (id, log_) =>
            println (log_)
            log.info (s"Task $id failed")
            orchDaemon ! DaemonProto.TaskFailure (id, log_)
        case DaemonProto.RmTaskDir (id, user) =>
            val path = Path.build (Path.build (Path (Global.user_home), user), "" + id).file
            // log.info (s"Rm $id dir : $path")
            // val file = new java.io.File (path)
            // deleteRecursively (file)
        case DaemonProto.RmTaskInput (id, user, path_) =>
            val path = Path.build (Path.build (Path.build (Path (Global.user_home), user), "" + id), path_).file
            // log.info (s"Rm $path file")
            // deleteRecursively (new java.io.File (path))
    }

    def deleteRecursively(file: java.io.File): Unit = {
        // if (file.isDirectory)
        //     file.listFiles.foreach(deleteRecursively)

        // if (file.exists) 
        //     file.delete
    }   

    override def postStop () : Unit = {
        //println ("Daemon stopped")
    }

}

object Daemon {
    import scopt.OParser
    import com.orch.daemon.Options._
    import com.orch.file._

    def props (addr : String, port : Int) : Props = Props (new Daemon (addr, port))

    def configFile (addr: String, port: Int): String = {
        s"""akka {
      loglevel = "INFO"
      log-dead-letters-during-shutdown=off
      log-dead-letters=off
      actor {
        warn-about-java-serializer-usage=off
        provider = "akka.remote.RemoteActorRefProvider"
      }
      remote {
        log-remote-lifecycle-events=off
        enabled-transports = ["akka.remote.netty.tcp"]
        netty.tcp {
          hostname = $addr
          port = $port
        }
        log-sent-messages = on
        log-received-messages = on
      }
    }"""
    }

    def parseOptions (args : Array[String]) : Options.Config = {
        OParser.parse(Options.parser1, args, Options.Config()).getOrElse (Options.Config ())
    }

    def main (args : Array[String]) {
        val config = parseOptions (args)
        Global.vm_path = config.vm
        Global.user_home = config.home

        val file_config = ConfigFactory.parseString (configFile (config.addr, config.port))
        val system = ActorSystem ("RemoteSystem", file_config)
        val masterActor = system.actorOf (Daemon.props (config.addr, config.port), name="daemon")
    }
}
