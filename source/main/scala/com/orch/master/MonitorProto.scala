package com.orch.master.MonitorProto

object MonitorProto {

    /**
      * Enregistrement d'un nouveau compute
      */
    case class AddDaemon  (addr : String, port : Int, cluster: String, name: String, speed : Int, capas: Map[String, Int])
    case class AddCluster (name : String, bw : Map [String, Int])
    case class NoDaemon   (id : String)

    /**
      * Enregistrement d'un superviseur
      */
    case class AddMonitor (addr : String, port : Int)
    case class RmMonitor  (addr : String, port : Int)

    /**
      * Message sur les VM
      */
    case class LaunchVM   (id : Long, flavor : Long, user : String, node : String)
    case class KillVM     (id : Long)
    case class PauseVM    (id : Long)
    case class ResumeVM   (id : Long)
    case class DelVM      (id : Long)
    case class VmReady    (id : Long, log : String)
    case class VmResumed  (id : Long)
    case class VmError    (id : Long, log : String)
    case class VmOff      (id : Long, log : String)
    case class NoVm       (id : Long)
    case class VmIdUsed   (id : Long)

    /**
      * Message sur les flavor
      */
    case class AddFlavor  (id : Long, os : String, capas : Map[String, Int], script : String)
    case class NoFlavor   (id : Long)
    case class FlavIdUsed (id : Long)

    /**
      * Message sur les execs
      */
    case class UploadExec   (id : String, path : String)
    case class RunTask      (id : Long, exec : String, vm : Long, params : String)
    case class ReRunTask    (id : Long, vm : Long, params : String)
    case class NoExec       (id : String)
    case class NoTask       (id : Long)
    case class TaskEnd      (id : Long)
    case class TaskFailure  (id : Long, log : String)
    case class RmTaskDir    (id : Long)
    case class RmTaskInputs (id : Long)

    /**
      * Message sur les fichiers
      */
    case class SendFile    (r_idSrc : Long, r_idDst : Long, node : String, task : Long)
    case class AddFile     (f_id : Long, filename : String, user : String, size : Int)
    case class ReplicaReg  (r_id : Long, f_id : Long, node : String, task : Long)
    case class ReplicaRegSend  (r_id : Long, f_id : Long, node : String, task : Long)
    case class NoReplica   (r_id : Long)
    case class NoFile      (f_id : Long)
    case class FileSent    (r_id : Long)
    case class FileFailure (r_id : Long)

    case class RegisterInput (f_id : Long, path : String)
    case class SendInput (f_id : Long, r_idDst : Long, node : String, task : Long)
    case class DownloadOuput (r_id : Long, r_idDst : Long, path : String)

}
