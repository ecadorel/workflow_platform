package com.orch.master

import java.io.File
import scala.Array
import akka.actor.{Props, Actor, ActorSystem, ActorRef, ActorSelection, ActorLogging, PoisonPill }
import akka.remote.DisassociatedEvent
import com.typesafe.config.ConfigFactory
import scala.io.Source
import java.net.InetSocketAddress
import com.orch.utils._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class Master (MasterAddr : String, MasterPort : Int) extends Actor with ActorLogging {
    import com.orch.master.MonitorProto._
    import com.orch.daemon.DaemonProto._
    import com.orch.file._
    import com.orch.db._

    var remoteDaemon: Map[String, ActorSelection] = Map ()
    var remoteMonitor: Array[ActorSelection] = Array ()
    var coll = new Collection ("orch")
    var fast = new FastLocalCollection ()

    override def preStart () : Unit = {
        log.info ("Orch master is running")
        context.system.eventStream.subscribe(self, classOf[DisassociatedEvent]);
        FileIO.startServer ()
    }

    def receive : Receive = {
        /**
          * Connexion daemon
          */
        case MonitorProto.AddDaemon (addr, port, cluster, name, speed, capas) => {
            try {
                log.info (s"try to connect to new compute node $addr:$port, $cluster/$name of $speed Mhz, $capas")
                val remote = context.actorSelection (s"akka.tcp://RemoteSystem@$addr:$port/user/daemon")
                remoteDaemon = remoteDaemon.filterKeys (k => k != name)
                log.info ("New daemon connected : " + remote)
                remote ! DaemonProto.RegisterMaster (MasterAddr, MasterPort)
                remoteDaemon += (name -> remote)
                this.coll.insertOrReplace (Node (name, capas, speed, cluster, addr, port))
                this.coll.await ()
            } catch {
                case _ : Throwable => {}
            }
        }

        case MonitorProto.AddCluster (name, bw) =>
            this.coll.insertOrReplace (Cluster (name, bw))
            this.coll.await ()

        /**
          *  Gestion moniteur
          */
        case MonitorProto.AddMonitor (addr, port) => {
            val remote = context.actorSelection (s"akka.tcp://RemoteSystem@$addr:$port/user/monitor")
            log.info ("New monitor connected : " + remote.anchorPath.address.hostPort)
            remoteMonitor = remoteMonitor :+ remote
            this.coll.insertOrReplace (SchedulerInfo (System.currentTimeMillis ()));
        }
        case MonitorProto.RmMonitor (addr, port) => { // Le moniteur a quitté la connexion
            log.info (s"Closing remote monitor : $addr:$port")
            remoteMonitor = remoteMonitor.filter (act => act.anchorPath.address.hostPort != s"RemoteSystem@$addr:$port")
            this.coll.replicateFastCollection (this.fast)
            log.info ("Saved")
            self ! PoisonPill
            context.system.terminate ()
            println ("[1;35mEnd - Bye[0m")
            System.exit (0)
        }

        /**
          * Gestion des flavors
          */
        case MonitorProto.AddFlavor (id, os, capas, script) =>  addFlavor (id, os, capas, script) 

        /**
          * Gestion des VM
          */
        case MonitorProto.LaunchVM (id, flavor, user, node) =>  launchVM (id, flavor, user, node) 
        case MonitorProto.KillVM   (id) => killVM (id) 
        case MonitorProto.PauseVM  (id) => pauseVM (id) 
        case MonitorProto.ResumeVM (id) => resumeVM (id) 
        case MonitorProto.DelVM    (id) => delVM (id) 
        case DaemonProto.VmReady   (id, log) =>  onVmReady (id, log) 
        case DaemonProto.VmResumed (id)      => onVmResumed (id) 
        case DaemonProto.VmError   (id, log) => onVmError (id, log) 
        case DaemonProto.VmOff     (id, log) => onVmOff (id, log) 

        /**
          * Gestion des fichiers
          */
        case MonitorProto.AddFile    (id, filename, user, size) => addFile (id, filename, user, size) 
        case MonitorProto.ReplicaReg (id, f_id, node, task) =>  registerReplica (id, f_id, node, task) 
        case MonitorProto.ReplicaRegSend (id, f_id, node, task) => registerReplicaSending (id, f_id, node, task) 
        case MonitorProto.SendFile   (r_idSrc, r_idDst, node, task) => sendFile (r_idSrc, r_idDst, node, task) 
        case DaemonProto.FileSent    (id) => onFileSent (id) 
        case DaemonProto.FileFailure (id) =>  onFileFailure (id) 
        case MonitorProto.RegisterInput (f_id, path) => registerInput (f_id, path) 
        case MonitorProto.SendInput (f_id, r_idDst, node, task) => sendInput (f_id, r_idDst, node, task) 
        case MonitorProto.DownloadOuput (r_id, r_idDst, path) => downloadResult (r_id, r_idDst, path) 

        /**
          * Gestion des taches
          */
        case MonitorProto.UploadExec (id, path) => uploadExec (id, path) 
        case MonitorProto.RunTask    (id, e_id, vm, params) =>
            runTask (id, e_id, vm, params) 
        case MonitorProto.ReRunTask  (id, vm, params) => rerunTask (id, vm, params) 

        case FileProto.FileSent      (id) => onFileSent (id) 
        case FileProto.FileFailure    (id) => onFileFailure (id) 

        case FileProto.ExeSent       (id, params) => launchTask (id, params.asInstanceOf [String]) 
        case FileProto.ExeFailure    (id, e_id, v_id, params) =>
            runTask (id, e_id, v_id, params.asInstanceOf [String]) 

        // case FileProto.RecvFile      (addr, port, path) => FileIO.recvFile (addr, port, path)


        case DaemonProto.TaskEnd       (id) => onTaskEnd (id) 
        case DaemonProto.TaskFailure   (id, log) => onTaskFailure (id, log) 
        case MonitorProto.RmTaskDir    (id)      => onRmTaskDir (id) 
        case MonitorProto.RmTaskInputs (id)      => onRmTaskInputs (id) 
        /**
          * Evenement auxiliaires
          */
        case DisassociatedEvent (local, remote, inBound) =>
            log.info ("Disassociation " + remote)
            remoteDaemon = remoteDaemon.filter ((t) => t._2.anchorPath.address.hostPort != remote.hostPort)
            remoteMonitor = remoteMonitor.filter (act => act.anchorPath.address.hostPort != remote.hostPort)

        case e =>
            println ("Unhandled " + e)
    }

    /**
      * Flavors
      */

    def addFlavor (id : Long, os : String, capas : Map [String, Int], script : String) : Unit = {
        this.fast.insertOrReplace (Flavor (id, os, capas, script))    
    }

    /**
      *  VMS
      */
    def launchVM (id : Long, flavor : Long, user : String, node : String) : Unit = {
        if (!remoteDaemon.contains (node)) {
            sender () ! MonitorProto.NoDaemon (node)
        } else {
            try {
                val remote = remoteDaemon (node)
                val flav = this.fast.findFlavor (flavor)
                if (flav == null) {
                    Thread.sleep (100)
                    launchVM (id, flavor, user, node)
                    return
                } else {
                    log.info (s"Launch VM ${id}")
                    val time = System.currentTimeMillis()
                    this.fast.insertOrReplace (VM (id, node, flavor, user, VMState.BOOTING, time, 0, 0))
                    remote ! DaemonProto.LaunchVM (id, flav.os, flav.capas, user, flav.custom_script)
                }
            } catch {
                case e : Throwable =>
                    sender () ! MonitorProto.VmError (id, "impossible de boot")
            }
        }
    }

    def killVM (id : Long) : Unit = {
        val vm = this.fast.findVM (id)
        if (vm == null) {
            Thread.sleep (100)
            killVM (id)
            return
        } else if (!remoteDaemon.contains (vm.n_id)) {
            sender () ! MonitorProto.NoDaemon (vm.n_id)
        } else if (vm.state != VMState.DOWN) {
            val remote = remoteDaemon (vm.n_id)
            remote ! DaemonProto.KillVM (id)
        } else
              sender ! MonitorProto.VmOff (id, "")
    }

    def pauseVM (id : Long) : Unit = {
        val vm = this.fast.findVM (id)
        if (vm == null) {
            Thread.sleep (100)
            pauseVM (id)
            return
        } else if (!remoteDaemon.contains (vm.n_id)) {
            sender () ! MonitorProto.NoDaemon (vm.n_id)
        } else {
            val remote = remoteDaemon (vm.n_id)
            remote ! DaemonProto.PauseVM (id)
        } 
    }

    def resumeVM (id : Long) : Unit = {
        val vm = this.fast.findVM (id)
        if (vm == null) {
            Thread.sleep (100)
            resumeVM (id)
            return
        } else if (!remoteDaemon.contains (vm.n_id)) {
            sender () ! MonitorProto.NoDaemon (vm.n_id)
        } else {
            val remote = remoteDaemon (vm.n_id)
            remote ! DaemonProto.ResumeVM (id)
        } 
    }

    def delVM (id : Long) : Unit = {
        killVM (id)
        this.fast.deleteVM (id)
    }

    def onVmReady (id : Long, out : String) : Unit = {
        val vm = this.fast.findVM (id)
        if (vm == null) {
            log.info (s"[1;31m VM ${id} is ready - retry [0m")
            Thread.sleep (100)
            onVmReady (id, out)
            return
        } else {
            val nVm = VM (vm.v_id, vm.n_id, vm.f_id, vm.user, VMState.RUNNING, vm.start, System.currentTimeMillis(), 0)
            log.info (s"[1;34m VM ${id} is ready[0m")
            remoteMonitor foreach { t => t ! MonitorProto.VmReady (id, out) }
            this.fast.insertOrReplace (nVm)
        }
    }

    def onVmResumed (id : Long) : Unit = {
        val vm = this.fast.findVM (id)
        if (vm == null) {
            log.info (s"[1;31m VM ${id} is resumed - retry [0mm")
            Thread.sleep (100)
            onVmResumed (id)
            return
        } else {
            log.info (s"[1;34m VM ${id} is resumed[0m")
            remoteMonitor foreach { t => t ! MonitorProto.VmResumed (id) }
        }
    }

    def onVmError (id : Long, out : String) : Unit = {
        var vm = this.fast.findVM (id)
        if (vm == null) {
            log.info (s"[1;31m VM ${id} is error - retry [0m")
            Thread.sleep (100)
            onVmError (id, out)
            return
        } else {
            val nVm = VM (vm.v_id, vm.n_id, vm.f_id, vm.user, VMState.ERROR, vm.start, vm.ready, System.currentTimeMillis())
            log.info (s"[1;31m VM ${id} is error [0m")
            remoteMonitor foreach { t => t ! MonitorProto.VmError (id, out) }
            this.fast.insertOrReplace (nVm)
        }
    }

    def onVmOff (id : Long, out : String) : Unit = {
        var vm = this.fast.findVM (id)
        if (vm == null) {
            log.info (s"[1;31m VM ${id} is off - retry [0mm")
            Thread.sleep (100)
            onVmOff (id, out)
            return
        } else {
            val nVm = VM (vm.v_id, vm.n_id, vm.f_id, vm.user, VMState.DOWN, vm.start, vm.ready, System.currentTimeMillis())
            log.info (s"[1;34m VM ${id} is off[0m")
            remoteMonitor foreach { t => t ! MonitorProto.VmOff (id, out) }
            this.fast.insertOrReplace (nVm)
        }
    }

    /**
      * Files
      */

    def addFile (id : Long, filename : String, user : String, size : Int) : Unit = {
        this.fast.insertOrReplace (com.orch.db.File (id, filename, user, size))
    }

    def registerReplica (id : Long, f_id : Long, node : String, t_id : Long) : Unit = {
        val file = this.fast.findFile (f_id)
        val remote = remoteDaemon (node)
        if (remote == null) {
            Thread.sleep (100)
            registerReplica (id, f_id, node, t_id)
        } else if (file == null) {
            Thread.sleep (100)
            registerReplica (id, f_id, node, t_id)
        } else {
            val time = System.currentTimeMillis ()
            this.fast.insertOrReplace (Replica (id, file.f_id, node, t_id, ReplicaType.OUTPUT, FileState.SENT, time, time))
        }
    }

    def registerReplicaSending (id : Long, f_id : Long, node : String, t_id : Long) : Unit = {
        val file = this.fast.findFile (f_id)
        val remote = remoteDaemon (node)
        if (remote == null) {
            Thread.sleep (100)
            registerReplicaSending (id, f_id, node, t_id)
        } else if (file == null) {
            Thread.sleep (100)
            registerReplicaSending (id, f_id, node, t_id)
        } else {
            val time = System.currentTimeMillis ()
            this.fast.insertOrReplace (Replica (id, file.f_id, node, t_id, ReplicaType.INPUT, FileState.SENDING, time, Long.MaxValue))
        }
    }

    def registerInput (f_id : Long, path : String) : Unit = {
        this.fast.insertOrReplace (IOFile (f_id, path))
    }

    def sendInput (f_id : Long, r_idDst : Long, node : String, task : Long) : Unit = {
        val file = this.fast.findFile (f_id)
        val io = this.fast.findIO (f_id)
        if (file == null || io == null) {
            Thread.sleep (100)
            sendInput (f_id, r_idDst, node, task)
        } else {
            val time = System.currentTimeMillis ()
            val recvRemote = remoteDaemon (node)
            log.info (s"Send input ${r_idDst}") 

            this.fast.insertOrReplace (Replica (r_idDst, file.f_id, node, task, ReplicaType.INPUT, FileState.SENDING, time, 0))
            recvRemote ! DaemonProto.RecvInput (r_idDst, MasterAddr, task, file.user, io.path, file.filename)
        }
    }

    def downloadResult (r_id : Long, r_idDst : Long, path : String) : Unit = {
        val repl = this.fast.findReplica (r_id)
        if (repl == null) {
            Thread.sleep (100)
            downloadResult (r_id, r_idDst, path)
        } else {
            this.fast.insertOrReplace (IOFile (repl.f_id, path))
            val file = this.fast.findFile (repl.f_id)
            val sendRemote = remoteDaemon (repl.n_id)
            log.info (s"Send result ${r_idDst}/${path}")

            val srcPath = Path.build (Path.build (Path.build (Global.user_home, file.user), "" + repl.t_id), file.filename).file
            val dstPath = "output:" + path

            val time = System.currentTimeMillis ()
            this.fast.insertOrReplace (Replica (r_idDst, file.f_id, "output", -1, ReplicaType.RESULT, FileState.SENDING, time, 0))
            FileIO.recvFile (self, r_idDst, srcPath, dstPath, sendRemote.anchorPath.address.host.get)
        }
    }

    def sendFile (r_idSrc : Long, r_idDst : Long, node : String, t_id : Long) : Unit = {
        val repl = this.fast.findReplica (r_idSrc)
        if (repl == null) {
            Thread.sleep (100)
            sendFile (r_idSrc, r_idDst, node, t_id)
        } else {
            val time = System.currentTimeMillis ()
            val file = this.fast.findFile (repl.f_id)
            val recvRemote = remoteDaemon (node)
            val sendRemote = remoteDaemon (repl.n_id)
            val addr = sendRemote.anchorPath.address.host.get
            log.info (s"Send file ${r_idDst}")
            this.fast.insertOrReplace (Replica (r_idDst, file.f_id, node, t_id, ReplicaType.INPUT, FileState.SENDING, time, 0))
            recvRemote ! DaemonProto.RecvFile (r_idDst, addr, repl.t_id, t_id, file.user, file.filename, file.filename)
        }
    }

    def onFileSent (id : Long) : Unit = {        
        val repl = this.fast.findReplica (id)
        if (repl == null) {
            log.info (s"[1;34m Information about unknown replica : $id [0m")
            Thread.sleep (100)
            onFileSent (id)
        } else {
            val nRepl = Replica (repl.r_id, repl.f_id, repl.n_id, repl.t_id, repl.rtype, FileState.SENT, repl.creation, System.currentTimeMillis ())
            this.fast.insertOrReplace (nRepl)
            log.info (s"File sent : ${id}")
        }
        remoteMonitor foreach { t => t ! MonitorProto.FileSent (id) }    
    }

    def onFileFailure (id : Long) : Unit = {
        val repl = this.fast.findReplica (id)
        log.info ("[1;31mFile failure !![0m")
        if (repl == null) {
            log.info (s"[1;34m Information about unknown replica : $id [0m")
            Thread.sleep (100)
            onFileFailure (id)
        } else {
            val nRepl = Replica (repl.r_id, repl.f_id, repl.n_id, repl.t_id, repl.rtype, FileState.FAILURE, repl.creation, System.currentTimeMillis ())
            remoteMonitor foreach { t => t ! MonitorProto.FileFailure (id) }
            this.fast.insertOrReplace (nRepl)
        }
    }

    /**
      * Task
      */

    def uploadExec (id : String, path : String) {
        this.fast.insertOrReplace (Executable (id, path))
    }

    def runTask (id : Long, e_id : String, v_id : Long, params : String) {
        val task = this.fast.findTask (id)
        if (task != null && task.state == TaskState.RUNNING) {
            log.info (s"[1;34m Launch allready running task ${id}")
        }

        val vm = this.fast.findVM (v_id)
        val exec = this.fast.findExecutable (e_id)
        if (vm == null) {
            Thread.sleep (100)
            runTask (id, e_id, v_id, params)
            return
        } else if (exec == null) {
            Thread.sleep (100)
            runTask (id, e_id, v_id, params)
            return
        } else {
            val remote = remoteDaemon (vm.n_id)
            this.fast.insertOrReplace (Task (id, v_id, e_id, vm.user, TaskState.NONE, 0, 0))
            val dstPath = "file:" + vm.user + ":" + id + ":exec.tar"

            remote ! DaemonProto.RecvExe (id, e_id, params, exec.path, dstPath, MasterAddr)
        }
    }

    def rerunTask (id : Long, v_id : Long, params : String) {
        val vm = this.fast.findVM (v_id)
        val task = this.fast.findTask (id)
        val remote = remoteDaemon (vm.n_id)
        val time = System.currentTimeMillis ()
        this.fast.insertOrReplace (Task (id, task.v_id, task.e_id, task.user, TaskState.RUNNING, time, 0))
        remote ! DaemonProto.RunTask (id, vm.user, task.v_id, params)
    }

    def launchTask (id : Long, params : String) {
        val task = this.fast.findTask (id)
        val vm = this.fast.findVM (task.v_id)
        val remote = remoteDaemon (vm.n_id)
        val time = System.currentTimeMillis()
        this.fast.insertOrReplace (Task (id, task.v_id, task.e_id, task.user, TaskState.RUNNING, time, 0))
        remote ! DaemonProto.RunTask (id, vm.user, task.v_id, params)
    }

    def onTaskEnd (id : Long) : Unit = {
        var task = this.fast.findTask (id)
        if (task == null) {
            Thread.sleep (100)
            onTaskEnd (id)
            return
        } else {
            log.info (s"Task $id is finished")
            var nTask = Task (task.t_id, task.v_id, task.e_id, task.user, TaskState.FINISHED, task.start, System.currentTimeMillis())
            this.fast.insertOrReplace (nTask)
        }
        remoteMonitor foreach { t => t ! MonitorProto.TaskEnd (id) }        
    }

    def onTaskFailure (id : Long, out : String) : Unit = {
        var task = this.fast.findTask (id)
        if (task == null) {
            Thread.sleep (100)
            onTaskFailure (id, out)
            return
        } else {
            var vm = this.fast.findVM (task.v_id)
            log.info (s"[1;31mTask $id has failed $out on node ${vm.n_id} [0m")
            var nTask = Task (task.t_id, task.v_id, task.e_id, task.user, TaskState.FAILURE, task.start, System.currentTimeMillis())
            this.fast.insertOrReplace (nTask)
        }
        remoteMonitor foreach { t => t ! MonitorProto.TaskFailure (id, out) }        
    }

    def onRmTaskDir (id : Long) : Unit = {
        val task = this.fast.findTask (id)
        if (task != null) {
            val vm = this.fast.findVM (task.v_id)
            val remoteNode = remoteDaemon (vm.n_id)
            remoteNode ! DaemonProto.RmTaskDir (id, task.user)
        }
    }

    def onRmTaskInputs (id : Long) : Unit = {
        val task = this.fast.findTask (id)
        val vm = this.fast.findVM (task.v_id)
        val repls = this.fast.findAllReplicasForTask (vm.n_id, id)
        val remoteNode = remoteDaemon (vm.n_id)
        for (repl <- repls) {            
            if (repl.rtype == ReplicaType.INPUT) {
                val file = this.fast.findFile (repl.f_id)
                remoteNode ! DaemonProto.RmTaskInput (id, vm.user, file.filename)
                this.fast.insertOrReplace (Replica (repl.r_id, repl.f_id, repl.n_id, repl.t_id, ReplicaType.INPUT, FileState.NO_EXIST, repl.creation, repl.available))
            }
        }

        remoteNode ! DaemonProto.RmTaskInput (id, vm.user, "bin/")
        remoteNode ! DaemonProto.RmTaskInput (id, vm.user, "launch.sh")
        remoteNode ! DaemonProto.RmTaskInput (id, vm.user, "execs.tar")
    }

    /**
      *  
      */
    
    override def postStop () : Unit = {
        log.info ("Orch master has stopped")
    }

    def saveDatabase () : Unit = {
        while (true) {
            this.coll.replicateFastCollection (this.fast)
            println ("[1;35mdatabase saved[0m")        
            Thread.sleep (60000) // Toutes les minutes
        }
    }

}

object Master {
    import scopt.OParser
    import com.orch.master.Options._
    import com.orch.master.MonitorProto._

    def props (addr : String, port : Int) : Props = Props (new Master (addr, port))

    def configFile (addr: String, port: Int):String = {
        s"""akka {
      loglevel = "INFO"
      log-dead-letters-during-shutdown=off
      log-dead-letters=off
      actor {
        warn-about-java-serializer-usage=off
        provider = "akka.remote.RemoteActorRefProvider"
      }
      remote {
        log-remote-lifecycle-events=off
        enabled-transports = ["akka.remote.netty.tcp"]
        netty.tcp {
          hostname = $addr
          port = $port
        }
        log-sent-messages = on
        log-received-messages = on
      }
    }"""
    }

    def parseOptions (args : Array[String]) : Options.Config = {
        OParser.parse(Options.parser1, args, Options.Config()).getOrElse (Options.Config ())
    }

    def launchConfig (master : ActorRef, config : String) : Unit = {
        val list = Loader.parseNodeFile (config)
        for (node <- list) {
            master ! MonitorProto.AddDaemon (node.addr, node.port, node.cluster, node.n_id, node.speed, node.capas)
        }

        val cluss = Loader.parseClusterFile (config)
        for (cluster <- cluss) {
            master ! MonitorProto.AddCluster (cluster._1, cluster._2)
        }
    }

    def main (args: Array[String]) {
        val config = parseOptions (args)
        val file_config = ConfigFactory.parseString (configFile (config.addr, config.port))
        val system = ActorSystem ("RemoteSystem", file_config)
        val masterActor = system.actorOf (
            Master.props (config.addr, config.port), name="master"
        )

        launchConfig (masterActor, config.config)
    }
    
}
