package com.orch.scheduler


object Interval {
    import com.orch.utils._

    def min (a : Long, b : Long) : Long = {
        if (a < b) a
        else b
    }

    def max (a : Long, b : Long) : Long = {
        if (a < b) b
        else a
    }

    def intersect (a : Interval, b : Interval) : Interval = {
        val x = max (a.begin, b.begin)
        val y = min (a.end, b.end)
        if (x < y) Interval (x, y, a.height + b.height)
        else Interval (-1, -1, 0)
    }

    def isAlong (a : Interval, b : Interval) : Interval = {
        val x = max (a.begin, b.begin)
        val y = min (a.end, b.end)
        if (x <= y) Interval (x, y - x, a.height + b.height)
        else Interval (-1, -1, 0)
    }

    def firstIntersect (I : Interval, L : Seq [Interval]) : (List [Interval], List [Interval], Interval) = {
        var seg = I
        var irest = 0
        var icut = 1
        var toCut : List [Interval] = List (seg)
        var toRest : List [Interval] = List ()
        for (t <- L) {
            val inter = intersect (seg, t)
            if (inter.begin == -1) {
                toRest = t :: toRest
            } else {
                toCut = t :: toCut
                seg = inter
            }
        }
        (toCut, toRest, seg)
    }

    def allIntersect (L : Seq[Interval]) : Seq[Interval] = {
        var Qu = L.toList
        var inters : List [Interval] = List ()
        while (Qu.length > 0) {
            val seg = Qu (0)
            val (toCut, toRest, inter) = firstIntersect (seg, Qu.drop (1))
            inters = inter :: inters
            var R = toRest
            for (it <- toCut) {
                val left = Interval (it.begin, inter.begin , it.height)
                val right = Interval (inter.end, it.end, it.height)
                if (left.len > 0) R = left :: R
                if (right.len > 0) R = right :: R
            }
            Qu = R
        }
        inters.sortWith (_.begin < _.begin)
    }   

    def maxOver (L : Seq [Interval], max : Int) : Interval = {
        var end = Interval (-1, -1, max)
        for (z <- L) {
            if (z.height > end.height) {
                end = z
            }
        }
        end
    }

    // assume L is sorted
    def lastOver (start : Long, end : Long,  L : Seq [Interval], need : Int, max : Int) : Interval = {
        val test = Interval (start, end, need)
        var res = Interval (-1, -1, max)
        for (z <- L) {
            val intersect = Interval.intersect (z, test)
            if (z.begin > end) return res
            else if (intersect.begin != -1 && intersect.height > max) {
                res = z
            }
        }
        res
    }

    // assume L is sorted
    def firstOver (start : Long, end : Long,  L : Seq [Interval], need : Int, max : Int) : Interval = {
        val test = Interval (start, end, need)
        var res = Interval (-1, -1, max)
        for (z <- L) {
            val intersect = Interval.intersect (z, test)
            if (intersect.begin != -1 && intersect.height > max) {
                return z
            }
        }
        res
    }



    def heightAt (start : Long, L : Seq [Interval]) : Int = {
        for (z <- L.sortWith (_.begin < _.begin)) {
            if (z.begin <= start && z.end >= start)
                return z.height
        }
        return 0
    }

    def firstUnder (start : Long, L : Seq [Interval], H : Int) : Long = {
        var max = start
        for (l <- L.sortWith (_.begin < _.begin)) {
            if (l.height < H && l.begin >= start) return l.begin
            else if (l.begin >= start) max = l.end
        }
        max
    }

    def subset (L : Seq[Interval], start : Long, end : Long) : Seq [Interval] = {
        var res : List [Interval] = List ()
        val ref = Interval (start, end, 0)
        for (int <- L) {
            val intersect = Interval.intersect (ref, int)
            if (intersect.begin != -1) res = intersect :: res
            if (int.begin > end) return res
        }
        res
    }

    def maxIntervalOnIntersect (inters : Seq [Interval]) : Int = {
        if (inters.length != 0) {
            val x = inters.reduce ((a, b) => if (a.height < b.height) b else a)
            x.height
        } else 0
    }

    def maxIntersect (inters : Seq [Interval]) : Int = {
        val L = allIntersect (inters)
        maxIntervalOnIntersect (L)
    }

    def biggest (i : Interval, L : Seq [Interval]) : Interval = {
        var max = i
        for (l <- L) {
            val inter = intersect (i, l)
            if (inter.height > max.height)
                max = inter
        }
        max
    }

    def inverse (L : Seq [Interval], max : Int) : Seq [Interval] = {
        var currentEnd = Long.MaxValue
        var res : Seq [Interval] = Seq ()
        for (l <- L) {
            if (l.begin > currentEnd) {
                res = res :+ Interval (currentEnd, l.begin, max)
            }
            currentEnd = l.end
            val currMax = max - l.height
            if (currMax > 0) res = res :+ Interval (l.begin, l.end, currMax)
        }
        return res
    }

    def maxLen (L : Seq [Interval]) : Long = {
        var currLen : Long = 0
        var maxLen : Long = 0
        var currEnd : Long = 0
        for (l <- L) {
            if (currLen == 0) {
                currLen = (l.end - l.begin)
                if (maxLen < currLen) maxLen = currLen
            } else if (currEnd == l.begin - 1) {
                currEnd += (l.end - l.begin)
                if (maxLen < currLen) maxLen = currLen
            } else currLen = 0

            currEnd = l.end
        }
        return maxLen
    }

    def prettyFormat (L : Seq[Interval]) : String = {
        val n = maxIntersect (L)
        var buf = new StringBuilder
        var lines : Array [Array [Interval]] = new Array (n)
        for (i <- 1 to n) lines (i - 1) = Array ()
        var rtype = 1
        var maxEnd : Long = 0
        for (t <- L) {
            var nb = t.height
            var j = 0
            while (nb > 0 && j < lines.length) {
                val inter = biggest (t, lines (j))
                if (inter.height == t.height) {
                    lines (j) = lines (j) :+ Interval (max (1, t.begin), max (1, t.end), 1, if (t.rtype == -1) rtype else t.rtype)
                    nb -= 1
                }
                j += 1
            }

            if (t.end > maxEnd) maxEnd = t.end
            rtype %= 9
            rtype += 1
        }

        buf ++= "╔"
        for (it <- 0.toLong to 3) buf ++= "═"
        buf ++= "╦"
        for (it <- 5.toLong to (maxEnd - 1) * 4 + 8) buf ++= ("═")
        buf ++= "╗\n"
        var i = 0
        for (it <- lines) {
            i += 1
            val line = Formatter.center (s"$i", 4, '.')
            buf ++= "│" + line + "│" + toStr (it, maxEnd) + "│\n"
        }

        buf ++= "│"
        for (it <- 0 to 3) buf ++= " "
        buf ++= "│"
        for (it <- 0.toLong to maxEnd - 1)
            buf ++= s"╷ ${it%10}╷"
        buf ++= "│\n"

        buf ++= "│"
        for (it <- 0 to 3) buf ++= " "
        buf ++= "│"
        for (it <- 0.toLong to maxEnd - 1)
            if (it % 10 == 0)
                buf ++= s"╷ ${it/10%10}╷"
            else
                buf ++= "╷  ╷"

        buf ++= "│\n╚"
        for (it <- 0.toLong to 3) buf ++= "═"
        buf ++= "╩"
        for (it <- 5.toLong to ((maxEnd - 1) * 4 + 8))   buf ++= "═"
        buf ++= "╝\n"
        buf.toString
    }

    def toStr (L : Array [Interval], max : Long) : String = {
        val line = L.sortWith ((a, b) => a.begin < b.begin)
        var buf = new StringBuilder
        var current : Long = 0
        for (t <- line) {
            if (current < t.begin) {
                for (j <- current until t.begin)
                    buf ++= "\033[09m    \033[m"
            }
            buf ++= s"├\u001B[1;4${t.rtype%10}m"
            val inner = Formatter.center (s"${t.rtype}", (t.len * 4 - 2).toInt, '═')
            buf ++= inner + "\u001B[0m┤"
            current = t.end
        }

        for (it <- current to max - 1)
            buf ++= "\033[09m    \033[m"
        buf.toString
    }

}

case class Interval (begin : Long, end : Long, height : Int, rtype : Int = -1) {

    def len () : Long = {
        end - begin
    }

    def withStartEnd (begin : Long, end : Long) : Interval = {
        Interval (begin, end, height, rtype)
    }

}
