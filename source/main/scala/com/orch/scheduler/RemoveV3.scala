package com.orch.scheduler
import akka.actor.{Props, Actor, ActorSystem, ActorRef, ActorLogging, ActorSelection, PoisonPill }
import scala.math.{max, min, ceil}
import com.orch.db._
import util.control.Breaks._
import com.orch.utils._

import com.orch.scheduler._


object V3Speed {
    val CONVERGENCE = 1000
    var ALGO_TYPE = REMOVE_ALGO_TYPES.N1
    var KILLING = 0
    var OBJ_TYPE = REMOVE_ALGO_TYPES.SPEED
}

class RemoveV3MetaData (val submissionTime : Long, val deadline : Long, val rank : Long, var anchor : Long, var speed : Int = 0) extends MetaData {

    def submitDead () : Long = {
        submissionTime + deadline
    }

    def isTotallySchedulable (t : SchedTask) : Boolean = {
        for (x <- t.work.pred) {
            if (x.task != null && (x.task.loc == null || (x.task.loc.state != TaskState.FINISHED && x.task.loc.state != TaskState.RUNNING)))
                return false
        }
        return true
    }

    override def isSchedulable (t : SchedTask) : Boolean = {
        if (V3Speed.ALGO_TYPE != REMOVE_ALGO_TYPES.ALL && V3Speed.ALGO_TYPE != REMOVE_ALGO_TYPES.AN) {
            return isTotallySchedulable (t)
        } else if (V3Speed.ALGO_TYPE == REMOVE_ALGO_TYPES.AN) {
            for (x <- t.work.pred) {
                if (x.task != null && !isTotallySchedulable (x.task))
                    return false
            }
        }

        return true
    }


    override def toString () : String = {
        return "R(d : " + deadline + ", a : " + anchor + ", r : " + rank + ")"
    }
}

class RemoveV3Scheduler (parent : ScheduleActor, ref : ActorSelection, RAddr : String, RPort : Int) extends Scheduler (parent, ref, RAddr, RPort) {

    override def onNewLoad (load : List [Workflow]) : Unit = {
        this.load ++= load
        for (f <- load) {
            removeV3ComputeDeadlines (f, infra)
        }
    }

    /**
      * ***************************
      * ALGO PRINCIPAL
      * ***************************
      */

    override def scheduleTasks (tasks : Seq [SchedTask], mutex : Integer) : Seq [SchedTask] = {
        println (s"Schedule Remove : ${tasks.size}")
        if (tasks.length == 0) {
            printNodes ()
            return tasks
        }

        var Q = tasks
        var all : Map [Long, SchedTask] = Map ()
        var current = System.currentTimeMillis () / 1000

        mutex.synchronized {
            do {
                var max_end = computeMaxEnd (infra.nodes)
                print (s"\b\b\b\b\b\b\b\b\b${Q.length}")
                Q = Q.sortWith (_.meta.asInstanceOf [RemoveV3MetaData].submitDead < _.meta.asInstanceOf [RemoveV3MetaData].submitDead)
                val t = Q (0);

                all += (t.id -> t)
                Q = Q.slice (1, Q.length);

                var (rm_loc, removed) = scheduleComplexAndReturnBest (current, t, infra.nodes, max_end)
                Q = Q ++ applyRemoveAll (removed)
                rm_loc.apply ()
                val rm_current = System.currentTimeMillis () / 1000
                val anch = t.meta.asInstanceOf [RemoveV3MetaData].submitDead - (rm_current + rm_loc.end)
                anchorTask (t, anch)

                if (anch < 0) {
                    Q = Q ++ subRank (t)
                }

            } while (Q.length > 0)
        }

        all.values.toSeq
    }

    def computeMaxEnd (nodes : Map [String, SchedNode]) : Long = {
        var end : Long = 0
        for (n <- nodes) {
            val loc_end = n._2.getEnd ()
            if (end < loc_end)
                end = loc_end
        }
        end
    }

    def applyRemoveAll (list : Seq [SchedTask]) : Seq [SchedTask] = {
        var all_removed : Map [Long, SchedTask] = Map ()
        for (x <- list) {
            if (x.loc != null) {
                val app_list = x.loc.totalRelease ()
                for (z <- app_list) {
                    if (!all_removed.contains (z.id)) {
                        all_removed = all_removed + (z.id -> z)
                        z.meta.asInstanceOf [RemoveV3MetaData].speed += 1
                    }
                }
            }
        }

        all_removed.values.toSeq
    }

    def subRank (t : SchedTask) : Seq [SchedTask] = {
        if (V3Speed.ALGO_TYPE == REMOVE_ALGO_TYPES.N2 || V3Speed.ALGO_TYPE == REMOVE_ALGO_TYPES.N3) {
            var list : Seq [SchedTask] = Seq ()
            for (x <- t.work.succ) {
                if (x.task != null) {
                    if (isFailingSchedulable (x.task))
                        list = list :+ x.task
                }
            }
            list
        } else Seq ()
    }

    def isFailingSchedulable (t : SchedTask, second : Boolean = false) : Boolean = {
        if (t.loc != null) return false
        if (V3Speed.ALGO_TYPE == REMOVE_ALGO_TYPES.N2) {
            for (x <- t.work.pred) {
                if (x.task != null && (x.task.loc == null || (x.task.loc.state == TaskState.NONE && x.task.meta.asInstanceOf[RemoveV3MetaData].anchor != Long.MinValue)  || !x.task.isSchedulable ()
                ))
                    return false
            }
        } else {
            for (x <- t.work.pred) {
                if (x.task != null && (x.task.loc == null || (x.task.loc.state == TaskState.NONE && x.task.meta.asInstanceOf[RemoveV3MetaData].anchor != Long.MinValue)
                ))
                    return false
            }
        }
        return true
    }




    /**
      * ***************************
      * OBJECTIFS
      * ***************************
      **/

    def chooseBestLocation (deadline : Long, locs : Seq [LocTask], consos : Seq [Long], max_end : Long) : (Boolean, Integer) = {
        var bestId = -1
        var best : LocTask = null
        var respect : Boolean = false
        var wasted : Long = Long.MaxValue
        val current = System.currentTimeMillis () / 1000
        var current_consos : Map [String, Long] = Map ()
        var current_glob_conso : Long = 0
        for (n <- infra.nodes) {
            val cur_conso = computeConso (n._2, max_end)
            current_consos = current_consos + (n._1 -> cur_conso)
            current_glob_conso += cur_conso
        }

        for (i <- 0 until locs.length) {
            val l = locs (i)
            val success = (current + l.end) < deadline
            val conso = consos (i) + current_glob_conso - current_consos (l.vm.node.name)
            if (success) {
                if (wasted > conso || !respect) {
                    bestId = i
                    best = l
                    wasted = conso
                    respect = true
                }
            } else if (!respect) {
                if (best == null || (l.end < best.end) ) {
                    bestId = i
                    best = l
                    wasted = conso
                }
            }
        }
        (respect, bestId)
    }

    def chooseBestSpeed (deadline : Long, locs : Seq [LocTask], consos : Seq [Long], max_end : Long) : (Boolean, Integer) = {
        var bestId = -1
        var best : LocTask = null

        val current = System.currentTimeMillis () / 1000

        for (i <- 0 until locs.length) {
            val l = locs (i)
            if (best == null || (l.end < best.end) ) {
                bestId = i
                best = l                
            }
        }
        (true, bestId)
    }

    def chooseBestLocationComplex (deadline : Long, locs : Seq [LocTask], consos : Seq [Long], removes : Seq [Seq [SchedTask]], max_end : Long) : (Boolean, Integer) = {
        var bestId = -1
        var best : LocTask = null
        var respect : Boolean = false
        val current = System.currentTimeMillis () / 1000
        var nb_remove : Long = Long.MaxValue

        for (i <- 0 until locs.length) {
            val l = locs (i)
            val success = current + l.end < deadline
            if (success) {
                if (removes (i).length < nb_remove  || !respect) {
                    bestId = i
                    best = l
                    nb_remove = removes (i).length
                    respect = true
                }
            } else if (!respect) {
                if (best == null || (l.end < best.end) ) {
                    bestId = i
                    best = l
                    nb_remove = removes (i).length
                }
            }
        }
        (respect, bestId)
    }


    /**
      * ***************************
      * SCHEDULE SIMPLE
      * ***************************
      */

    def scheduleSimpleAndReturnBest (task : SchedTask, nodes : Map [String, SchedNode], max_end : Long) : (Boolean, LocTask) = {
        var pos : Seq [LocTask] = Seq ()
        var conso : Seq [Long] = Seq ()
        for (n <- nodes) {
            val minStart = minStartTime (task, n._2)
            val loc = scheduleOnNodeSimple (task, n._2, minStart)
            pos = pos :+ loc
            conso = conso :+ computeConso (n._2, max_end, loc)
        }

        val (respect, id) = if (V3Speed.OBJ_TYPE == REMOVE_ALGO_TYPES.ENERGY)
            chooseBestLocation (task.meta.asInstanceOf [RemoveV3MetaData].submitDead, pos, conso, max_end)
        else chooseBestSpeed (task.meta.asInstanceOf [RemoveV3MetaData].submitDead, pos, conso, max_end)
        return (respect, pos (id))
    }

    def scheduleOnNodeSimple (task : SchedTask, node : SchedNode, minStart : Long) : LocTask = {
        var loc : LocTask = null
        val zero = currentTime ()


        for (v <- node.getVMs) {
            var aux_loc = v._2.getPlace (zero, minStart, task)
            if (aux_loc != null) {
                if (loc == null) loc = aux_loc
                else if (loc.end > aux_loc.end) loc = aux_loc
            }
        };

        val n_vm = new SchedVM (IdGenerator.nextId (), node, task.work.user, task.work.os, computeBoot (task.work.os) / node.getSpeed)

        val aux_loc = n_vm.getPlaceOnNewVM (zero, minStart, task)
        if (aux_loc != null) {
            if (loc == null) {
                loc = aux_loc
            } else if (loc.end > aux_loc.end) {
                loc = aux_loc
            }
        }

        loc
    }

    /**
      * ***************************
      * SCHEDULE COMPLEX
      * ***************************
      */

    def scheduleComplexAndReturnBest (current : Long, task : SchedTask, nodes : Map [String, SchedNode], max_end : Long) : (LocTask, Seq [SchedTask]) = {
        var pos : Seq [LocTask] = Seq ()
        var removed : Seq [Seq [SchedTask]] = Seq ()
        var consos : Seq [Long] = Seq ()
        for (n <- nodes) {
            val minStart = minStartTime (task, n._2)
            val (loc, rm, conso) = scheduleOnNodeComplex (current, task, n._2, minStart, max_end)
            pos = pos :+ loc
            removed = removed :+ rm
            consos = consos :+ conso
        }
        val (respect, id) = if (V3Speed.OBJ_TYPE == REMOVE_ALGO_TYPES.ENERGY)
            chooseBestLocation (task.meta.asInstanceOf [RemoveV3MetaData].submitDead, pos, consos, max_end)
        else chooseBestSpeed (task.meta.asInstanceOf [RemoveV3MetaData].submitDead, pos, consos, max_end)
        
        return (pos (id), removed (id))
    }

    def scheduleOnNodeComplex (current : Long, task : SchedTask, node : SchedNode, minStart : Long, max_end : Long) : (LocTask, Seq [SchedTask], Long) = {
        var list = getAllUnder (current, task, node, minStart)
        val allVms = node.getVMs ()
        val allTasks = node.getAllTasks ()
        node.setFast (true)
        for (t <- list) {
            t.loc.releaseFast ()
        }

        for (i <- allVms) {
            i._2.fullValidate ()
        }
        node.computeUsage ()
        node.setFast (false)

        val loc = scheduleOnNodeSimple (task, node, minStart)
        val consoIfAdd : Long = computeConso (node, max_end, loc)

        node.setFast (true) // Idem que pour remove
                            // Restore
        node.purge ()
        for (t <- allTasks) {
            if (t._2.loc != null) {
                t._2.loc.applyFast ()
            }
        }
        for (i <- allVms) {
            i._2.fullValidate ()
        }
        node.computeUsage ()
        node.setFast (false)// On remet comme il faut


        (loc, list, consoIfAdd)
    }

    def anchorTask (t : SchedTask, anch : Long) : Unit = {
        if (t.meta.asInstanceOf [RemoveV3MetaData].speed > V3Speed.CONVERGENCE && anch < 0 && anch != Long.MinValue)
            anchorTask (t, Long.MinValue)

        if (t.meta.asInstanceOf [RemoveV3MetaData].anchor > anch) {
            t.meta.asInstanceOf [RemoveV3MetaData].anchor = anch
            for (p <- t.work.pred) {
                if (p.task != null) anchorTask (p.task, anch)
            }
        }
    }

    def getAllUnder (current : Long, task : SchedTask, node : SchedNode, minStart : Long) : Seq [SchedTask] = {
        val rank = task.meta.asInstanceOf [RemoveV3MetaData].submitDead - current
        var res : Seq [SchedTask] = Seq ()
        for (v <- node.getVMs) {
            if (V3Speed.KILLING == 1 || v._2.getState () == VMState.DOWN) {
                for (t <- v._2.getTasks) {
                    val t_rank = t._2.meta.asInstanceOf [RemoveV3MetaData].submitDead - current
                    if (t_rank > rank && t._2.loc.state == TaskState.NONE)
                        res = res :+ t._2
                }
            }
        }
        res
    }

    def getAllUnder (task : SchedTask, anch : Long, vm : SchedVM, minStart : Long) : Seq [SchedTask] = {
        var res : Seq [SchedTask] = Seq ()
        for (t <- vm.getTasks) {
            if (t._2.meta.asInstanceOf [RemoveV3MetaData].anchor > anch && t._2.loc.state == TaskState.NONE && t._2.loc.end >= minStart && t._2.meta.asInstanceOf [RemoveV3MetaData].speed < V3Speed.CONVERGENCE)
                res = res :+ t._2        
        }
        res
    }

    /**
      * ***************************
      *  DEADLINES
      * ***************************
      */


    def removeV3ComputeDeadlines (flow : Workflow, infra : Infrastructure) : Unit = {
        val sub = System.currentTimeMillis () / 1000
        val entry = flow.tasks.values.filter (_.isEntry ())
        for (t <- entry) {
            removeV3ComputeDeadlines (t, infra, sub, flow.deadline)
        }
    }

    def removeV3ComputeDeadlines (task : SchedTask, infra : Infrastructure, subTime : Long, deadline : Long) : (Long, Long) = {
        if (task.meta != null) {
            (task.meta.asInstanceOf [RemoveV3MetaData].deadline.toLong, task.meta.asInstanceOf [RemoveV3MetaData].rank.toLong)
        } else {
            var max : Long = deadline
            var max_rank : Long = 0
            var pass : Boolean = false
            for (s <- task.work.succ) {
                if (s.task != null) {
                    pass = true
                    val (dead, rank) = removeV3ComputeDeadlines (s.task, infra, subTime, deadline)
                    var mean_com = 0
                    for (n <- infra.nodes) for (m <- infra.nodes) {
                        val current = ceil ((s.file.size * 10) / infra.bw (n._2.cluster)(m._2.cluster)).toInt
                        mean_com += current
                    }

                    mean_com /= (infra.nodes.size * infra.nodes.size)
                    if (dead - mean_com < max) max = dead - mean_com
                    if (rank + mean_com > max_rank) max_rank = rank + mean_com
                }
            }

            val len = task.work.computeLen (Utils.PROBA).toInt
            var mean_time = 0
            for (n <- infra.nodes)
                mean_time += (ceil ((len / (n._2.getSpeed * Utils.SPEED_DEGRAD))).toLong + Utils.TIME_ORDER).toInt
            mean_time /= infra.nodes.size
            mean_time += 1


            task.meta = new RemoveV3MetaData (subTime, deadline - max_rank, max_rank + mean_time, 0)
            (deadline - max_rank, max_rank + mean_time)
            
        }
    }

    /**
      * ***************************
      * ENERGIE
      * ***************************
      */

    def computeConso (node : SchedNode, max_end : Long, loc : LocTask = null) : Long = {
        val usage = node.getCurrentUsage ()
        var global_usage : Seq [Interval] = if (usage.contains ("cpus"))
            usage ("cpus")
        else Seq ()

        global_usage = global_usage :+ Interval (0, max_end, 0)

        if (loc != null) {
            global_usage = global_usage :+ Interval (loc.start, loc.end, loc.task.work.needs ("cpus"))
        }

        global_usage = Interval.allIntersect (global_usage)
        var conso : Long = 0
        for (i <- global_usage) {
            var load = i.height.toFloat / node.getCapas() ("cpus").toFloat
            var instantConso = math.ceil (node.getMaxConso () + (node.getIdleConso () - node.getMaxConso ()) / math.log (0.001) * math.log (load)).toInt
            conso = conso + instantConso * (i.end - i.begin).toInt
        }

        conso
    }


}
