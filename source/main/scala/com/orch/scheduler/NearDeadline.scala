package com.orch.scheduler
import akka.actor.{Props, Actor, ActorSystem, ActorRef, ActorLogging, ActorSelection, PoisonPill }
import scala.math.{max, min, ceil}
import com.orch.db._
import util.control.Breaks._
import com.orch.utils._

import com.orch.scheduler._



object ND_ALGO_TYPES {
    val RATIO = 1
    val GLOB = 2

    var ALGO_TYPE = ND_ALGO_TYPES.RATIO
}

/**
  *  Le rank est le temps pris par les predecesseur
  *  Le forward rank est le temps pris par les successeur
*/
class NDMetaData (val submissionTime : Long, var deadline : Long, val inv_deadline : Long, val rank : Long, var mean_time : Long, var forwardRank : Long = 0, var best : Boolean = false) extends MetaData { 

    def submitDead () : Long = {
        submissionTime + deadline
    }

    def currentDead (current : Long) : Long = {
        submitDead
    }

    // Une tâche qui passe dans F, ne peux pas retourner dans Q
    def setBest () : Unit = {
        this.best = true 
    }

    /**
      *  La deadrank se caclule en fonction du temps restant pour les successeurs, c'est juste une approximation moyenne
      */
    def deadRank () : Long = {
        deadline - forwardRank
    }

    def currentDeadRank (current : Long) : Long = {
        currentDead (current) - forwardRank
    }

    def submitForwardRank () : Long = {
        forwardRank + submissionTime
    }


    def isTotallySchedulable (t : SchedTask) : Boolean = {
        for (x <- t.work.pred) {
            if (x.task != null && (x.task.loc == null || (x.task.loc.state != TaskState.FINISHED && x.task.loc.state != TaskState.RUNNING)))
                return false
        }
        return true
    }

    override def isSchedulable (t : SchedTask) : Boolean = {
        // if (NDSpeed.ALGO_TYPE != REMOVE_ALGO_TYPES.ALL && NDSpeed.ALGO_TYPE != REMOVE_ALGO_TYPES.AN) {
        //     return isTotallySchedulable (t)
        // } else if (V3Speed.ALGO_TYPE == REMOVE_ALGO_TYPES.AN) {
        //     for (x <- t.work.pred) {
        //         if (x.task != null && !isTotallySchedulable (x.task))
        //             return false
        //     }
        // }

        return true
    }

    override def toString () : String = {
        return "R(d : " + deadline + ", r : " + rank + ", fr : " + forwardRank + ")"
    }
}

class NearDeadlineScheduler (parent : ScheduleActor, ref : ActorSelection, RAddr : String, RPort : Int) extends Scheduler (parent, ref, RAddr, RPort, SchedType.ON_TIME, false) {

    override def onNewLoad (load : List [Workflow]) : Unit = {
        this.load ++= load
        for (f <- load) {
            ndComputeDeadlines (f, infra)
        }
    }

    /**
      * ***************************
      * ALGO PRINCIPAL BY TASKS
      * ***************************
      */

    override def scheduleTasks (tasks : Seq [SchedTask], mutex : Integer) : Seq [SchedTask] = {
        println ("ND does not provide by task scheduling")
        System.exit (-1)
        return tasks
    }

    /**
      * ***************************
      * ALGO PRINCIPAL BY FLOWS
      * ***************************
      */
    override def scheduleFlows (flows : Seq [Workflow], mutex : Integer) : Seq [SchedTask] = {
        var Q = flows
        var F : Seq [Workflow] = Seq ()

        val zero = currentTime ()
        mutex.synchronized {
            while (Q.length != 0) {
                Q = Q.sortWith (entryMinDeadRank (zero, _) < entryMinDeadRank (zero, _))
                breakable {
                    while (Q.length != 0) {
                        Formatter.printForwardLine (s"Q : ${Q.length}:${F.length}")
                        val f = Q (0)
                        println ("Slow : " + f.user)
                        Q = Q.slice (1, Q.length)

                        if (!scheduleFlow (zero, f)) {
                            println ("Invalidate")
                            invalidateFlow (f)
                            F = F :+ f
                            break;
                        }
                    }
                }
                println ("")                
                if (F.length != 0) {
                    F = F.sortWith (entryMinDeadRank (zero, _) < entryMinDeadRank (zero, _))
                    println ("Removing")
                    printNodes ()
                    val (_QList, _FList) = removeAllUnder (zero, F (0))
                    printNodes ()

                    F = (_FList ++ F).sortWith (entryMinDeadRank (zero, _) < entryMinDeadRank (zero, _))
                    Q = Q ++ _QList

                    while (F.length != 0) {
                        Formatter.printForwardLine (s"F : ${F.length}:${Q.length}")
                        val f = F (0)
                        println ("Fast : " + f.user)
                        F = F.slice (1, F.length)
                        scheduleFlowBestEffort (zero, f)
                    }
                    println ("")
                }

            }
        }

        println (s"Schedule finished on ${flows.size}")
        printNodes ()

        return allTasks (flows)
    }

    override def isSchedulable (flow : Workflow) : Boolean = {
        for (t <- flow.tasks) {
            if (t._2.loc != null)
                return false
        }
        return true
    }

    def allTasks (flows : Seq [Workflow]) : Seq [SchedTask] = {
        var lst : Seq [SchedTask] = List ()
        for (x <- flows) {
            for (t <- x.tasks)
                lst = lst :+ t._2
        }
        lst
    }

    /**
      * ***************************
      * ALGO PRINCIPAL UTILS
      * ***************************
      */

    def entryMinDeadRank (current : Long, flow : Workflow) : Long = {
        var res : Long = Long.MaxValue
        for (x <- flow.tasks) {
            val deadRank = x._2.meta.asInstanceOf [NDMetaData].currentDeadRank (current)
            if (deadRank < res && x._2.isEntry ()) {
                res = deadRank
            }
        }
        res
    }

    def entryMinDeadRankTask (current : Long, flow : Workflow) : SchedTask = {
        var res : Long = Long.MaxValue
        var task : SchedTask = null
        for (x <- flow.tasks) {
            val deadRank = x._2.meta.asInstanceOf [NDMetaData].currentDeadRank (current)
            if (deadRank < res && x._2.isEntry ()) {
                res = deadRank
                task = x._2
            }
        }
        task
    }

    def applyRemoveAll (list : Seq [SchedTask]) : (Seq [SchedTask], Seq [SchedTask]) = {
        var all_removed : Map [Long, SchedTask] = Map ()
        var vms : Map [Long, SchedVM] = Map ()
        for (x <- list) {
            if (x.loc != null && x.loc.state == TaskState.NONE) {
                val (app_list, vm_list) = x.loc.totalReleaseFast ()
                for (z <- app_list) {
                    all_removed = all_removed + (z.id -> z)                    
                }

                for (v <- vm_list) {
                    vms = vms + (v.id -> v)
                }
            }
        }

        for (v <- vms) {
            v._2.validatePostRemoveFast ()
        }

        var F : Seq [SchedTask] = List ()
        var Q : Seq [SchedTask] = List ()
        for (x <- all_removed) {
            if (x._2.meta.asInstanceOf [NDMetaData].best)
                F = F :+ x._2
            else Q = Q :+ x._2
        }

        (Q, F)
    }


    def invalidateFlow (f : Workflow, Q : Seq [SchedTask]) : (Seq [SchedTask], Seq [SchedTask]) = {
        val Q_ = Q.filter (x => !f.tasks.contains (x.id))
        for (x <- f.tasks) {
            if (x._2.loc != null && x._2.loc.state == TaskState.NONE) x._2.loc.totalRelease ()
        }

        return (Q_, f.tasks.values.to [Seq])
    }

    def invalidateFlow (f : Workflow) : Unit = {
        var all_removed : Map [Long, SchedTask] = Map ()
        var vms : Map [Long, SchedVM] = Map ()
        for (x <- f.tasks) {
            if (x._2.loc != null && x._2.loc.state == TaskState.NONE) {
                val (app_list, vm_list) = x._2.loc.totalReleaseFast ()
                for (v <- vm_list) {
                    vms = vms + (v.id -> v)
                }
            }
        }

        for (v <- vms) {
            v._2.validatePostRemoveFast ()
        }
    }

    /**
      * ********************************
      *  SCHEDULE FLOW ON NODES DEADLINE
      * ********************************
      */
    def scheduleFlow (zero : Long, flow : Workflow) : Boolean = {
        val tasks = flow.tasks.values.to [Seq].sortWith (_.meta.asInstanceOf [NDMetaData].rank > _.meta.asInstanceOf [NDMetaData].rank)
        var i = 1
        for (t <- tasks) {
            if (t.loc == null) {
                if (!scheduleTask (zero, t))
                    return false
            }
        }
        return true
    }

    /**
      * ***********************************
      *  SCHEDULE FLOW ON NODES BEST EFFORT
      * ***********************************
      */

    def removeAllUnder (current : Long, flow : Workflow) : (Seq [Workflow], Seq[Workflow]) = {
        var Q : Map [Long, Workflow] = Map ()
        var F : Map [Long, Workflow] = Map ()
        for (n <- infra.nodes) {
            val list = getAllUnder (current, flow, n._2)
            val (_QList, _FList) = applyRemoveAll (list)

            for (t <- _QList) {
                if (!Q.contains (t.getFamily ().id))
                    Q = Q + (t.getFamily ().id -> t.getFamily ())
            }

            for (t <- _FList) {
                if (!F.contains (t.getFamily ().id))
                    F = F + (t.getFamily ().id -> t.getFamily ())
            }
        }
        return (Q.values.to [Seq], F.values.to [Seq])
    }

    def getAllUnder (current : Long, flow : Workflow, node : SchedNode) : Seq [SchedTask] = {
        val rank = entryMinDeadRank (current, flow)
        var res : Seq [SchedTask] = Seq ()
        for (v <- node.getVMs) {
            for (t <- v._2.getTasks) {
                val t_rank = entryMinDeadRank (current, t._2.getFamily ())

                if ((t_rank > rank || !t._2.meta.asInstanceOf [NDMetaData].best) && t._2.loc.state == TaskState.NONE && t._2.loc.end >= current)
                    res = res :+ t._2
            }
        }
        res
    }

    def scheduleFlowBestEffort (zero : Long, flow : Workflow) : Unit = {
        val tasks = flow.tasks.values.to [Seq].sortWith (_.meta.asInstanceOf [NDMetaData].forwardRank > _.meta.asInstanceOf [NDMetaData].forwardRank)
        for (t <- tasks) {
            if (t.loc == null)
                scheduleTaskBestEffort (zero, t)
        }
    }

    /**
      * ********************************
      *  SCHEDULE TASK ON NODES DEADLINE
      * ********************************
      */

    def scheduleTask (zero : Long, task : SchedTask) : Boolean = {
        var loc : LocTask = null        
        val max_end = computeMaxEnd (infra.nodes)
        for (node <- infra.nodes) {
            val aux_loc = scheduleTaskOnNode (zero, task, node._2)
            loc = checkObjectif (max_end, aux_loc, loc)            
        }

        if (loc != null) {
            loc.apply ()
            return true
        } else { // Deadline missed
                 // Ici il faut faire le systeme de suppression
                 // On backtrack au exit tasks, on supprime toutes les tâches qui ont un rank < et on reschedule avec une deadline superieur
                 // La complexité va vite explosé si jamais on a des deadlines très serré
            return false
        }
    }

    def scheduleTaskOnNode (zero : Long, task : SchedTask, node : SchedNode) : LocTask = {
        var loc : LocTask = null
        val maxDead = maxDeadTask (task, node)
        val minStart = partialMinStartTime (zero, task, node)
        val len = ceil (task.work.computeLen (Utils.PROBA).toInt / node.getSpeed * Utils.SPEED_DEGRAD + Utils.TIME_ORDER).toLong

        for (v <- node.getVMs) {
            var aux_loc = getPlaceNDVM (zero, v._2, minStart, maxDead, task, len)
            loc = checkObjectifVM (task, aux_loc, loc)
        }

        if (loc == null) {
            val n_vm = new SchedVM (IdGenerator.nextId (), node, task.work.user, task.work.os, computeBoot (task.work.os) / node.getSpeed ())
            getPlaceNDNewVM (zero, n_vm, minStart, maxDead, task, len)
        } else loc
    }

    /**
      * ************************************
      *  SCHEDULE TASK ON NODES BEST EFFORT
      * ************************************
      */

    def removeAllUnder (current : Long, task : SchedTask) : (Seq [SchedTask], Seq [SchedTask]) = {
        var Q : Seq [SchedTask] = List ()
        var F : Seq [SchedTask] = List ()
        for (n <- infra.nodes) {
            val list = getAllUnder (current, task, n._2)
            val (_QList, _FList) = applyRemoveAll (list)
            Q = Q ++ _QList
            F = F ++ _FList
        }

        (Q, F)
    }

    def scheduleTaskBestEffort (zero : Long, task : SchedTask) : Unit = {
        var loc : LocTask = null
        task.meta.asInstanceOf [NDMetaData].setBest ()

        val max_end = computeMaxEnd (infra.nodes)

        for (n <- infra.nodes) {
            val minStart = minStartTime (task, n._2)
            val aux_loc = scheduleOnNodeBestEffort (zero, task, n._2, minStart)
            loc = checkObjectifBestEffort (task, aux_loc, loc)
        }

        loc.apply ()
        loc.vm.setBestEffort ()
    }

    def getAllUnder (current : Long, task : SchedTask, node : SchedNode) : Seq [SchedTask] = {
        val rank = task.meta.asInstanceOf [NDMetaData].currentDeadRank (current)
        var res : Seq [SchedTask] = Seq ()
        for (v <- node.getVMs) {
            for (t <- v._2.getTasks) {
                val t_rank = t._2.meta.asInstanceOf [NDMetaData].currentDeadRank (current)

                // On ne peux pas supprimer des tâches du même workflows
                if (t_rank > rank && t._2.loc.state == TaskState.NONE && t._2.loc.end >= current)
                    res = res :+ t._2
            }
        }
        res
    }

    def scheduleOnNodeBestEffort (zero : Long, task : SchedTask, node : SchedNode, minStart : Long) : LocTask = {
        var loc : LocTask = null

        for (v <- node.getVMs) {
            var aux_loc = v._2.getPlace (zero, minStart, task)
            loc = checkObjectifBestEffort (task, aux_loc, loc)
        };

        if (loc == null) {
            val n_vm = new SchedVM (IdGenerator.nextId (), node, task.work.user, task.work.os, computeBoot (task.work.os) / node.getSpeed)
            val aux_loc = n_vm.getPlaceOnNewVM (zero, minStart, task)
            loc = checkObjectifBestEffort (task, aux_loc, loc)
        }

        loc
    }


    /**
      * ***************************
      *  SCHEDULE TASK ON VM
      * ***************************
      */

    def getPlaceNDVM (current : Long, vm : SchedVM, zero : Long, dead : Long, task : SchedTask, len : Long) : LocTask = {
        if (!vm.canRun (task)) return null

        // 2 phase, la première deadline -> zero
        //        , la deuxième -> deadline ++

        val LST : Long = dead - len // LAST START TIME
        val nodeUsage = vm.node.getUsageWithout (vm)
        val nodeCapas = vm.node.getCapas ()

        var position : Long = LST        
        while (position >= zero + vm.getBootTime ()) {
            val loc = new LocTask (position, position + len, TaskState.NONE, task, vm)
            val over = getCollisionOnVM (true, loc, task, vm, nodeUsage, nodeCapas)

            if (over.begin == -1) return loc
            else if (over.begin == -2) return null
            else if (position <= over.begin - len)
                return null
            else position = over.begin - len            
        }

        return null
    }

    /**
      * Params : 
      * - first = firstOver ou lastOver
      * Returns : l'interval de collision
      */
    def getCollisionOnVM (first : Boolean, loc : LocTask, task : SchedTask, vm : SchedVM, nodeUsage : Map [String, Seq [Interval]], nodeCapas : Map [String, Int]) : Interval = {
        val (start, end, newUsage, newCapas) = vm.fakeValidate (loc)
        val vmCapasIfAdd = if (vm.getState () == VMState.DOWN) newCapas else if (vm.isValid (newCapas)) vm.capas else newCapas

        val over =
            if (first) vm.firstOver (start - vm.getBootTime (), end, vmCapasIfAdd, nodeUsage, nodeCapas)
            else vm.lastOver (start - vm.getBootTime (), end, vmCapasIfAdd, nodeUsage, nodeCapas)
        
        if (over.begin == -1 && vm.isValid (newCapas)) return over
        else if (diffCapas (vm.getCapas (), newCapas)) {
            val max_capas = if (vm.getState () == VMState.DOWN)
                vm.computeMaxCapas (start, end, vm.getCapas (), nodeUsage, nodeCapas)
            else vm.getCapas ()

            if (max_capas == null) { // La VM est coince
                return Interval (-2, 0, 0)
            }

            val inner_over =
                if (first) vm.firstOver (loc.start, loc.end, task.work.needs, vm.getUsage (), max_capas)
                else vm.lastOver (loc.start, loc.end, task.work.needs, vm.getUsage (), max_capas)

            if (inner_over.begin != -1)
                return inner_over
        }

        return over
    }

    def diffCapas (left : Map [String, Int], right : Map [String, Int]) : Boolean = {
        for (x <- left) {
            if (!right.contains (x._1) || right (x._1) != x._2) return true
        }
        return false
    }

    def getPlaceNDNewVM (current : Long, vm : SchedVM, zero : Long, dead : Long, task : SchedTask, len : Long) : LocTask = {
        val LST : Long = dead - len
        val nodeUsage = vm.node.getUsageWithout (vm)
        val nodeCapas = vm.node.getCapas ()

        var position = LST
        while (position >= zero + vm.getBootTime ()) {
            val loc = new LocTask (position, position + len, TaskState.NONE, task, vm)
            val over = getCollisionOnNewVM (true, loc, task, vm, nodeUsage, nodeCapas)
            if (over.begin == -1) return loc
            else if (position < over.begin - len)
                position = zero
            else position = over.begin - len
        }

        return null
    }

    def getCollisionOnNewVM (first : Boolean, loc : LocTask, task : SchedTask, vm : SchedVM, nodeUsage : Map [String, Seq [Interval]], nodeCapas : Map [String, Int]) : Interval = {
        val (start, end, newUsage, newCapas) = vm.fakeValidate (loc)
        val vmCapasIfAdd = newCapas

        val over =
            if (first) vm.firstOver (start - vm.getBootTime (), end, vmCapasIfAdd, nodeUsage, nodeCapas)
            else vm.lastOver (start - vm.getBootTime (), end, vmCapasIfAdd, nodeUsage, nodeCapas)

        return over
    }

    /**
      * ***************************
      *  OBJECTIFS
      * ***************************
      */

    def sum(l: Seq[Long]): Long = {
        var x : Long = 0
        for (l_ <- l)
            x = x + l_
        return x
    }

    def checkObjectif (max_end : Long, left : LocTask, right : LocTask) : LocTask = {
        if (left == null) return right
        else if (right == null) return left

        return checkObjectif (left.task, left, right)
    }

    def checkObjectif (task : SchedTask, left : LocTask, right : LocTask) : LocTask = {
        if (left == null) return right
        if (right == null) return left

        val dead = task.meta.asInstanceOf [NDMetaData].deadline
        val left_diff =  (dead - left.end)/Utils.ALPHA
        val right_diff = (dead - right.end)/Utils.ALPHA

        val (leftRatio, rightRatio) =
            if (ND_ALGO_TYPES.ALGO_TYPE == ND_ALGO_TYPES.RATIO) {
                val (lst, led, lnU, lnC) = left.vm.fakeValidate (left)
                val (rst, red, rnU, rnC) = right.vm.fakeValidate (right) 
                val leftRatio : Float = (lnC ("cpus").toFloat)
                val rightRatio : Float = (rnC ("cpus").toFloat)

                (1.0f/leftRatio, 1.0f/rightRatio)
            } else {
                val leftRatio : Float = 1.0f / (left.vm.tasks.size+1);
                val rightRatio : Float = 1.0f / (right.vm.tasks.size+1);
                (leftRatio, rightRatio)
            }

        if (leftRatio < rightRatio)
            return left
        else if (leftRatio > rightRatio)
            return right
        else if (left_diff < right_diff) return left
        else return right
        


        // val distanceA = Math.abs (Math.sqrt (((0.0 - left_diff) * (0.0 - left_diff)) + ((0.0 - leftRatio) * (0.0 - leftRatio))));
        // val distanceB = Math.abs (Math.sqrt (((0.0 - right_diff) * (0.0 - right_diff)) + ((0.0 - rightRatio) * (0.0 - rightRatio))));

        // if (distanceA < distanceB)
        //     return left
        // else return right
    }


    /**
      * L'objectif est d'être le plus proche possible de la deadline,
      * mais être à gauche est mieux qu'être à droite
      */
    def checkObjectifVM (task : SchedTask, left : LocTask, right : LocTask) : LocTask = {
        return checkObjectif (task, left, right);
    }

    def checkObjectifBestEffort (task : SchedTask, left : LocTask, right : LocTask) : LocTask = {
        if (left == null) return right
        if (right == null) return left

        val left_diff =  (left.end)/Utils.ALPHA
        val right_diff = right.end/Utils.ALPHA

        val (leftRatio, rightRatio) =
            if (ND_ALGO_TYPES.ALGO_TYPE == ND_ALGO_TYPES.RATIO) {
                val (lst, led, lnU, lnC) = left.vm.fakeValidate (left)
                val (rst, red, rnU, rnC) = right.vm.fakeValidate (right)

                val leftRatio_ : Double =  (lnC ("cpus").toFloat)
                val rightRatio_ : Double = (rnC ("cpus").toFloat)
                // if (leftRatio == rightRatio) {
                //     if (left.end < right.end) return left
                //     else return right
                // } else if (leftRatio > rightRatio) return left
                // else return right
                (1.0/leftRatio_, 1.0/rightRatio_)
            } else {
                (1.0/(left.vm.tasks.size+1), 1.0/(right.vm.tasks.size+1))
                // if (left.vm.tasks.size == right.vm.tasks.size) {
                //     if (left.end < right.end) return left
                //     else return right
                // } else if (left.vm.tasks.size > right.vm.tasks.size) {
                //     return left
                // } else return right
            }

        val distanceA = Math.abs (Math.sqrt (((0.0 - left_diff) * (0.0 - left_diff)) + ((0.0 - leftRatio) * (0.0 - leftRatio))));
        val distanceB = Math.abs (Math.sqrt (((0.0 - right_diff) * (0.0 - right_diff)) + ((0.0 - rightRatio) * (0.0 - rightRatio))));

        
        if (distanceA < distanceB)
            return left
        else return right
    }


    /**
      * ***************************
      *  DEADLINES
      * ***************************
      */

    def maxDeadTask (task : SchedTask, node : SchedNode) : Long = {
        var time : Long = task.meta.asInstanceOf [NDMetaData].deadline
        for (edge <- task.work.succ) {
            if (edge.task != null) {
                val start = edge.task.loc.start
                val bw = infra.bw (edge.task.loc.vm.node.cluster) (node.cluster)
                val bw_time = ceil ((edge.file.size // * 10
                ) / bw.toDouble).toLong
                if (time > start - bw_time) time = start - bw_time
            }
        }
        time
    }

    def ndComputeDeadlines (flow : Workflow, infra : Infrastructure) : Unit = {
        val sub = startSchedule + flow.start
        println ("SUB : " + sub + " " + (System.currentTimeMillis () / 1000))
        val realDead = flow.deadline 
        for (t <- flow.tasks) {
            if (t._2.isOut ())
                ndComputeDeadlines (t._2, infra, sub, realDead)
        }

        for (t <- flow.tasks) {
            if (t._2.isEntry ())
                ndComputeForwardRank (t._2, infra)
        }

    }

    def ndComputeDeadlines (task : SchedTask, infra : Infrastructure, subTime : Long, deadline : Long) : Long = {
        if (task.meta != null) {
            task.meta.asInstanceOf [NDMetaData].rank.toLong
        } else {
            var max_rank : Long = 0
            var max_rank_no_com : Long = 0
            var pass : Boolean = false
            for (s <- task.work.pred) {
                if (s.task != null) {
                    pass = true
                    val rank = ndComputeDeadlines (s.task, infra, subTime, deadline)
                    val rank_no_com = s.task.meta.asInstanceOf [NDMetaData].inv_deadline + s.task.meta.asInstanceOf [NDMetaData].mean_time

                    var mean_com = 0
                    for (n <- infra.nodes) for (m <- infra.nodes) {
                        val current = ceil ((s.file.size // * 10
                        ) / infra.bw (n._2.cluster)(m._2.cluster)).toInt
                        mean_com += current
                    }

                    mean_com /= (infra.nodes.size * infra.nodes.size)
                    if (rank + mean_com > max_rank) max_rank = rank + mean_com
                    if (rank_no_com > max_rank_no_com) max_rank_no_com = rank_no_com
                }
            }

            val boot = computeBoot (task.work.os)
            val len = task.work.computeLen (Utils.PROBA).toInt
            var mean_time = 0
            for (n <- infra.nodes) {
                mean_time += (ceil ((len / (n._2.getSpeed * Utils.SPEED_DEGRAD))).toLong + Utils.TIME_ORDER).toInt
                val boot_node : Long = ceil (boot/ n._2.getSpeed * Utils.SPEED_DEGRAD).toLong;
                if (boot_node > max_rank_no_com)
                    max_rank_no_com = boot_node
            }

            mean_time /= infra.nodes.size
            mean_time += 1


            task.meta = new NDMetaData (subTime, deadline, max_rank_no_com, max_rank + mean_time, mean_time)
            max_rank + mean_time            
        }
    }


    def ndComputeForwardRank (task : SchedTask, infra : Infrastructure) : Long = {
        if (task.meta.asInstanceOf [NDMetaData].forwardRank != 0) {
            task.meta.asInstanceOf [NDMetaData].forwardRank
        } else {
            var max : Long = 0
            for (s <- task.work.succ) {
                if (s.task != null) {
                    val rank = ndComputeForwardRank (s.task, infra)
                    var mean_com : Long = 0
                    for (n <- infra.nodes) for (m <- infra.nodes) {
                        mean_com += ceil((s.file.size // * 10
                        ) / infra.bw (n._2.cluster)(m._2.cluster)).toInt
                    }
                    mean_com /= (infra.nodes.size * infra.nodes.size)
                    
                    if (rank + mean_com > max) max = rank + mean_com
                }
            }

            val len = task.work.computeLen (Utils.PROBA).toInt
            var mean_time = 0
            for (n <- infra.nodes)
                mean_time += (ceil ((len / (n._2.getSpeed * Utils.SPEED_DEGRAD))).toLong + Utils.TIME_ORDER).toInt
            mean_time /= infra.nodes.size
            mean_time += 1

            task.meta.asInstanceOf [NDMetaData].forwardRank = max + mean_time
            task.meta.asInstanceOf [NDMetaData].deadline = task.meta.asInstanceOf [NDMetaData].deadline - max
            max + mean_time
        }
    }


    /**
      * ***************************
      * ENERGIE
      * ***************************
      */

    def computeMaxEnd (nodes : Map [String, SchedNode]) : Long = {
        var end : Long = 0
        for (n <- nodes) {
            val loc_end = n._2.getEnd ()
            if (end < loc_end)
                end = loc_end
        }
        end
    }


    def computeConso (node : SchedNode, max_end : Long, loc : LocTask = null) : Long = {
        val usage = node.getCurrentUsage ()
        var global_usage : Seq [Interval] = if (usage.contains ("cpus"))
            usage ("cpus")
        else Seq ()

        global_usage = global_usage :+ Interval (0, max_end, 0)

        if (loc != null) {
            global_usage = global_usage :+ Interval (loc.start, loc.end, loc.task.work.needs ("cpus"))
        }

        global_usage = Interval.allIntersect (global_usage)
        var conso : Long = 0
        for (i <- global_usage) {
            var load = i.height.toFloat / node.getCapas() ("cpus").toFloat
            var instantConso = math.ceil (node.getMaxConso () + (node.getIdleConso () - node.getMaxConso ()) / math.log (0.001) * math.log (load)).toInt
            conso = conso + instantConso * (i.end - i.begin).toInt
        }

        conso
    }

    def partialMinStartTime (zero : Long, task : SchedTask, node : SchedNode) : Long = {
        var time : Long = zero
        for (edge <- task.work.pred) {
            if (edge.task != null && edge.task.loc != null) {
                val end = edge.task.loc.end
                val bw = infra.bw (edge.task.loc.vm.node.cluster) (node.cluster)
                val bw_time = ceil ((edge.file.size // * 10
                ) / bw.toDouble).toLong

                if (time < end + bw_time)
                    time = end + bw_time            
            } else {
                val bw = infra.bw ("input")(node.cluster)
                val bw_time = ceil ((edge.file.size // * 10
                ) / bw.toDouble).toLong

                if (time < bw_time)
                    time = bw_time
            }
        }

        if (task.meta.asInstanceOf[NDMetaData].inv_deadline > time)
            task.meta.asInstanceOf[NDMetaData].inv_deadline
        else 
            time
    }



}

