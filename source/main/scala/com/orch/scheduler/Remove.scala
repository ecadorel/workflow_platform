package com.orch.scheduler
import akka.actor.{Props, Actor, ActorSystem, ActorRef, ActorLogging, ActorSelection, PoisonPill }
import scala.math.{max, min, ceil}
import com.orch.db._
import util.control.Breaks._
import com.orch.utils._

import com.orch.scheduler._

object Speed {
    val CONVERGENCE = 50
}

class RemoveMetaData (val submissionTime : Long, val meanTime : Long, val deadline : Long, var anchor : Long, val rank : Long, var speed : Int = 0) extends MetaData {

    def submitDead () : Long = {
        submissionTime + deadline
    }

    override def isSchedulable (t : SchedTask) : Boolean = {
        // for (x <- t.work.pred) {
        //     if (x.task != null && (x.task.loc == null || x.task.loc.state != TaskState.FINISHED))
        //         return false
        // }

        return true
    }


    override def toString () : String = {
        return "R(d : " + deadline + ", a : " + anchor + ", r : " + rank + ")"
    }
}


class RemoveScheduler (parent : ScheduleActor, ref : ActorSelection, RAddr : String, RPort : Int) extends Scheduler (parent, ref, RAddr, RPort) {

    def uniform (low : Int, high : Int) : Int = {
        val x = scala.util.Random.nextInt % (high - low)
        if (x < 0) -x + low
        else x + low
    }

    val SPEED = 5

    override def onNewLoad (load : List [Workflow]) : Unit = {
        this.load ++= load
        for (f <- load) {
            removeComputeDeadlines (f, infra)
        }
    }

    override def scheduleTasks (tasks : Seq [SchedTask], mutex : Integer) : Seq [SchedTask] = {
        println (s"Schedule Remove : ${tasks.size}")
        if (tasks.length == 0) {
            printNodes ()
            return tasks
        }

        var Q = tasks
        var nbFailure = 0
        var nbPass = 0
        var all : Map [Long, SchedTask] = Map ()
        var current = System.currentTimeMillis () / 1000

        for (t <- tasks) {
            t.meta.asInstanceOf [RemoveMetaData].anchor = t.meta.asInstanceOf [RemoveMetaData].submitDead () - current;
        }

        var nbPlace = 0
        mutex.synchronized {
            do {
                print (s"\b\b\b\b\b\b\b\b\b${Q.length}/${nbFailure}")
                Q = Q.sortWith (_.meta.asInstanceOf [RemoveMetaData].rank > _.meta.asInstanceOf [RemoveMetaData].rank)
                val t = Q (0);

                all += (t.id -> t)
                Q = Q.slice (1, Q.length);

                var removed : Seq[SchedTask] = Seq ()
                var wasted = Int.MaxValue;
                var locTask : LocTask = null;
                var metaData : Long = 1
                var currentConso : Map [String, Int] = Map ()
                var respect : Boolean = false
                current = System.currentTimeMillis () / 1000

                for (n <- infra.nodes) {
                    currentConso = currentConso + (n._1 -> computeConso (n._2))
                }

                for (n <- infra.nodes) {
                    val min_start_time = minStartTime (t, n._2)
                    val begin = System.currentTimeMillis ()
                    val (success, local_waste, loc, meta, local_removed) = scheduleOnNode (t, n._2, currentConso (n._1), min_start_time)

                    if (success) {
                        var local_respect = current + loc.end < t.meta.asInstanceOf [RemoveMetaData].submitDead
                        if (local_respect) {
                            // if (local_removed.size < removed.size || !respect) {
                            //     locTask = loc
                            //     wasted = local_waste
                            //     removed = local_removed
                            //     metaData = meta
                            //     respect = true
                            // } else if (removed.size != 0 || !respect || local_removed.size == 0) {
                                if (wasted > local_waste || !respect) {
                                    locTask = loc
                                    wasted = local_waste
                                    removed = local_removed
                                    metaData = meta
                                    respect = true
                                }
                        //}
                        } else if (!respect) {
                            if ((locTask != null && loc.end < locTask.end) || locTask == null) {
                                locTask = loc
                                wasted = local_waste
                                removed = local_removed
                                metaData = meta
                            }
                        }
                    }

                    val end = System.currentTimeMillis ();
                }
                               

                var all_removed : Map [Long, SchedTask] = Map ()
                for (x <- removed) {
                    if (x.loc != null) { //
                        var list = x.loc.totalRelease ()
                        for (z <- list) {
                            if (!all_removed.contains (z.id)) {
                                z.meta.asInstanceOf [RemoveMetaData].speed += 1
                                all_removed = all_removed + (z.id -> z)
                            }
                        }
                    }
                }

                locTask.apply ()

                val curr = currentConso (locTask.vm.node.name)
                currentConso = currentConso + (locTask.vm.node.name -> (curr + wasted))
                // if (respect) { 
                // t.meta.asInstanceOf [RemoveMetaData].anchor =
                // }  else {
                anchorTask (t, (t.meta.asInstanceOf [RemoveMetaData].deadline) - locTask.end)
                //                }


                // if (!respect)
                //     for (x <- t.work.succ) {
                //         if (x.task != null) {
                //             if (HEFTScheduler.isFailingSchedulable (x.task))
                //                 Q = Q :+ x.task
                //         }
                //     }            

                for (x <- all_removed) {
                    if (x._2.isSchedulable ())
                        Q = Q :+ x._2
                }
                
            }
            while (Q.length > 0);
        }
                
        all.values.toSeq.foreach { x =>
            println (x.meta.asInstanceOf [RemoveMetaData].anchor,x.work.user)
        }

        println ("Schedule finished")
        printNodes ()
        all.values.toSeq
    }

    def anchorTask (t : SchedTask, anch : Long) : Unit = {
        if (t.meta.asInstanceOf [RemoveMetaData].anchor > anch) {
            t.meta.asInstanceOf [RemoveMetaData].anchor = anch
            for (p <- t.work.pred) {
                if (p.task != null) anchorTask (p.task, anch)
            }
        }
    }


    def scheduleOnNode (task : SchedTask, node : SchedNode, currentConso : Int, minStart : Long) : (Boolean, Int, LocTask, Long, Seq[SchedTask]) = {
        val (succ, loc) = scheduleOnNodeSimple (task, node, minStart)
        if (succ) {
            val consoIfAdd = computeConso (node, loc)
            (true, consoIfAdd - currentConso, loc, task.meta.asInstanceOf [RemoveMetaData].anchor, Seq ())
        } else {
            val anch = (task.meta.asInstanceOf [RemoveMetaData].deadline - loc.end)
            val (sec_loc, meta, consoIfAdd, list) = scheduleOnNodeComplex (task, node, minStart, anch)
            if (sec_loc != null) {
                (true, consoIfAdd - currentConso, sec_loc, meta, list)
            } else (false, 0, null, 0, Seq ())
        }
    }

    def scheduleOnNodeSimple (task : SchedTask, node : SchedNode, minStart : Long) : (Boolean, LocTask) = {
        var loc : LocTask = null
        val zero = currentTime ()


        for (v <- node.getVMs) {
            var aux_loc = v._2.getPlace (zero, minStart, task)
            if (aux_loc != null) {
                if (loc == null) loc = aux_loc
                else if (loc.end > aux_loc.end) loc = aux_loc
            }
        };

        var current = System.currentTimeMillis () / 1000
        val n_vm = new SchedVM (IdGenerator.nextId (), node, task.work.user, task.work.os, computeBoot (task.work.os) / node.getSpeed)

        val aux_loc = n_vm.getPlaceOnNewVM (zero, minStart, task)
        if (aux_loc != null) {
            if (loc == null) {
                loc = aux_loc
            } else if (loc.end > aux_loc.end) {
                loc = aux_loc
            }
        }

        (current + loc.end <= task.meta.asInstanceOf [RemoveMetaData].submitDead, loc)
    }

    def scheduleOnNodeComplex (task : SchedTask, node : SchedNode, minStart : Long, anch : Long) : (LocTask, Long, Int, Seq [SchedTask]) = {
        var list = removeAllUnder (task, anch, node, minStart)
        val allvms = node.getVMs ()
        val allTasks = node.getAllTasks ()

        node.setFast (true) // On veut pas une validation à chaque VM
        for (t <- list) {
            t.loc.releaseFast ()
        }

        for (i <- allvms) {
            i._2.fullValidate ()
        }
        node.computeUsage ()
        node.setFast (false)// On remet comme il faut pour le reste de l'algo

        var (succ, loc) = scheduleOnNodeSimple (task, node, minStart)

        var finalList : Seq [SchedTask] = Seq ()
        var consoIfAdd : Int = 0
        var anchor : Long = task.meta.asInstanceOf [RemoveMetaData].anchor - uniform (5, 10)
        consoIfAdd = computeConso (node, loc)        

        node.setFast (true) // Idem que pour remove
        // Restore
        node.purge ()
        for (t <- allTasks) {
            if (t._2.loc != null) 
                t._2.loc.applyFast ()
        }
        for (i <- allvms) {
            i._2.fullValidate ()
        }
        node.computeUsage ()
        node.setFast (false)// On remet comme il faut

        // Restore
        (loc, anchor, consoIfAdd, list)
    }

    def removeAllUnder (task : SchedTask, anch : Long, node : SchedNode, minStart : Long) : Seq [SchedTask] = {
        var res : Seq [SchedTask] = Seq ()
        for (v <- node.getVMs) {
            for (t <- v._2.getTasks) {
                if (t._2.meta.asInstanceOf [RemoveMetaData].anchor > anch && t._2.loc.state == TaskState.NONE && t._2.meta.asInstanceOf [RemoveMetaData].speed < Speed.CONVERGENCE && t._2.loc.end >= minStart)
                    res = res :+ t._2
            }
        }
        res
    }

    def removeComputeDeadlines (flow : Workflow, infra : Infrastructure) : Unit = {
        val sub = System.currentTimeMillis () / 1000
        val entry = flow.tasks.values.filter (_.isEntry ())
        for (t <- entry) {
            removeComputeDeadlines (t, infra, sub, flow.deadline)
        }
    }

    def computeConso (node : SchedNode, loc : LocTask = null) : Int = {
        val usage = node.getCurrentUsage ()
        var global_usage : Seq [Interval] = if (usage.contains ("cpus"))
            usage ("cpus")
        else Seq ()

        if (loc != null) {
            global_usage = global_usage :+ Interval (loc.start, loc.end, loc.task.work.needs ("cpus"))
        }

        global_usage = Interval.allIntersect (global_usage)
        var conso : Int = 0
        for (i <- global_usage) {
            var load = i.height.toFloat / node.getCapas() ("cpus").toFloat
            var instantConso = math.ceil (node.getMaxConso () + (node.getIdleConso () - node.getMaxConso ()) / math.log (0.001) * math.log (load)).toInt
            conso = conso + instantConso * (i.end - i.begin).toInt
        }

        conso
    }


    def removeComputeDeadlines (task : SchedTask, infra : Infrastructure, subTime : Long, deadline : Long) : (Long, Long) = {
        if (task.meta != null) {
            (task.meta.asInstanceOf [RemoveMetaData].deadline.toLong, task.meta.asInstanceOf [RemoveMetaData].rank.toLong)
        } else {
            var max : Long = Long.MaxValue
            var max_rank : Long = 0
            var pass : Boolean = false
            for (s <- task.work.succ) {
                if (s.task != null) {
                    pass = true
                    val (dead, rank) = removeComputeDeadlines (s.task, infra, subTime, deadline)
                    var mean_com = 0
                    for (n <- infra.nodes) for (m <- infra.nodes) {
                        val current = ceil ((s.file.size * 10) / infra.bw (n._2.cluster)(m._2.cluster)).toInt
                        mean_com += current
                    }
                    mean_com /= (infra.nodes.size * infra.nodes.size)
                    if (dead - mean_com < max) max = dead - mean_com
                    max_rank += rank + 1
                }
            }

            val len = task.work.computeLen (Utils.PROBA).toInt
            var mean_time = 0
            for (n <- infra.nodes)
                mean_time += (ceil ((len / (n._2.getSpeed * Utils.SPEED_DEGRAD))).toLong + Utils.TIME_ORDER).toInt
            mean_time /= infra.nodes.size
            mean_time += 1

            if (!pass) {
                task.meta = new RemoveMetaData (subTime, mean_time, deadline - max_rank, deadline - currentTime (), 0)
                (deadline, 0)
            } else {
                task.meta = new RemoveMetaData (subTime, mean_time, deadline - max_rank, max - currentTime (), max_rank)
                (max - mean_time, max_rank)
            }
        }
    }

    object HEFTScheduler {

        def scheduleTaskOnNode (task : SchedTask, node : SchedNode, minStart : Long) : LocTask = {
            var loc : LocTask = null
            val zero = currentTime ()
            for (v <- node.getVMs) {
                var aux_loc = v._2.getPlace (zero, minStart, task)
                if (aux_loc != null) {
                    if (loc == null) loc = aux_loc
                    else if (loc.end > aux_loc.end) loc = aux_loc
                }
            }

            
            val n_vm = new SchedVM (IdGenerator.nextId (), node, task.work.user, task.work.os, computeBoot (task.work.os) / node.getSpeed)
            val aux_loc = n_vm.getPlaceOnNewVM (zero, minStart, task)
            val end2 = System.currentTimeMillis ()
            if (aux_loc != null) {
                if (loc == null) {
                    loc = aux_loc
                } else if (loc.end > aux_loc.end) {
                    loc = aux_loc
                }
            }

            loc
        }

        def isFailingSchedulable (t : SchedTask, second : Boolean = false) : Boolean = {
            if (t.loc != null) return false
            for (x <- t.work.pred) {
                if (x.task != null && (x.task.loc == null || (x.task.loc.state == TaskState.NONE && x.task.meta.asInstanceOf[RemoveMetaData].anchor != Long.MinValue)  || !x.task.isSchedulable ()
                ))
                    return false
            }
            return true
        }
    }
}
