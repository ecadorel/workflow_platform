package com.orch.scheduler

class SchedNode (val name : String, val cluster : String, speed: Int, capas : Map [String, Int]) {
    import scala.math.{max}

    private var vms : Map [Long, SchedVM] = Map ()
    var ords : Seq [SchedVM] = Seq ()
    private var currentUsage : Map [String, Seq [Interval]] = Map ()
    private var fast : Boolean = false


    def addVM (vm : SchedVM) : Unit = {
        this.vms += (vm.id -> vm)
        if (!fast)
            computeUsage ()
    }

    def setFast (fast : Boolean) : Unit = {
        this.fast = fast
    }

    def getIdleConso () : Int = {
        65
    }

    def getMaxConso () : Int = {
        180
    }

    def removeVM (vm : SchedVM) : Unit = {
        this.vms = this.vms.filterKeys (_ != vm.id)
        computeUsage ()
    }

    def getCurrentUsage () : Map [String, Seq [Interval]] = {
        return currentUsage
    }

    def getVMs () : Map [Long, SchedVM] = {
        vms
    }

    def getAllTasks () : Map [Long, SchedTask] = {
        var res : Map [Long, SchedTask] = Map ()
        for (v <- vms) {
            res = res ++ v._2.getTasks ()
        }
        res
    }

    def purge () : Unit = {
        for (v <- vms) {
            v._2.purge ()
        }
        vms = Map ()
    }

    def getVM (id : Long) : Option [SchedVM] = {
        if (vms.contains (id)) Some (vms (id))
        else None
    }

    def computeUsage () : Unit = {
        var raw : Map [String, Seq [Interval]] = Map ()
        for (v <- vms) {
            if (v._2.end != 0) {
                for (x <- capas) {
                    val inter = v._2.toInterval (x._1)
                    // val begin = max (inter.begin, position)
                    // if (inter.end > position) {
                    if (raw.contains (x._1))
                        raw += (x._1 -> (raw (x._1) :+ inter))
                    else raw += (x._1 -> Seq (inter))
                }
            }
        }

        var res : Map [String, Seq [Interval]] = Map ()
        for (x <- raw) {
            res += (x._1 -> Interval.allIntersect (x._2))
        }
        currentUsage = res
    }

    def computeUsageCPU (): Seq[Interval] = {
        var raw : Seq[Interval] = Seq ()
        for (v <- vms) {
            if (v._2.getUsage.contains ("cpus")) {
                raw = raw ++ v._2.getUsage ("cpus")
            }
        }

        Interval.allIntersect (raw)
    }

    // assume that vm is in node
    def getUsageWithout (vm : SchedVM) : Map [String, Seq [Interval]] = {
        var res : Map [String, Seq [Interval]] = Map ()
        for (usage <- currentUsage) {
            if (vm.getCapas.contains (usage._1)) {
                var list : Seq [Interval] = Seq ()
                var interval = vm.toInterval (usage._1)
                for (l <- usage._2) {
                    val inter = Interval.intersect (l, interval)
                    if (inter.begin == -1) {                        
                        list = list :+ l
                    } else {
                        val left = Interval (l.begin, inter.begin, l.height)
                        val right = Interval (inter.end, l.end, l.height)
                        if (left.len > 0) list = list :+ left
                        if (right.len > 0) list = list :+ right
                        list = list :+ Interval (inter.begin, inter.end, l.height - interval.height)
                    }
                }
                res = res + (usage._1 -> list)
            } else res = res + (usage._1 -> usage._2)
        }
        res
    }

    def isValid () : Boolean = {
        for (x <- currentUsage) {
            if (capas (x._1) < Interval.maxIntervalOnIntersect (x._2))
                return false
        }
        for (v <- vms)
            if(!v._2.isValid ()) return false

        return true
    }

    def getCapas () : Map [String, Int] = {
        capas
    }

    def getSpeed () : Int = {
        speed
    }

    def getEnd () : Long = {
        if (this.currentUsage.contains ("cpus")) {
            this.currentUsage ("cpus").sortWith (_.end > _.end) (0).end
        } else 0
    }

    override def toString () : String = {
        name + " : " +  {
            if (vms.size > 1)
                vms.values.to[Seq].sortWith(_.start < _.start).map (_.toString ()).reduce (_ + _)
            else if (vms.size == 1) {
                val list : Seq [String] = vms.values.to[Seq].map (_.toString ())
                list (0)
            } else "{}"
        }
    }

}
