package com.orch.scheduler
import akka.actor.{Props, Actor, ActorSystem, ActorRef, ActorLogging, ActorSelection, PoisonPill }
import scala.math.{max, min, ceil}
import com.orch.db._
import util.control.Breaks._
import com.orch.utils._

class HEFTDeadMetaData (val submissionTime : Long, val meanTime : Long, val deadline : Long, var rank : Long) extends MetaData {
    val PESSIMISM : Float = 0.8f

    def submitDead () : Long = {
        submissionTime + deadline
    }

    def deadRank () : Long = {
        deadline - rank
    }

    override def toString () : String = {
        return "R(" + rank + ")"
    }

    override def isSchedulable (t : SchedTask) : Boolean = true
}

class HEFTDeadScheduler (parent : ScheduleActor, ref : ActorSelection, RAddr : String, RPort : Int) extends Scheduler (parent, ref, RAddr, RPort, SchedType.BEST_EFFORT, false) {

    override def onNewLoad (load : List [Workflow]) : Unit = {
        this.load ++= load
        for (f <- load) {
            HEFTDScheduler.heftDComputeRanks (f, infra)
        }
    }

    override def scheduleFlows (flows : Seq [Workflow], mutex : Integer) : Seq [SchedTask] = {
        println ("ici");
        var Q = flows
        mutex.synchronized {
            Q = Q.sortWith (entryMinDeadRank (_) < entryMinDeadRank (_))            
            while (Q.length != 0) {
                val f = Q (0)
                Q = Q.slice (1, Q.length)
                val tasks = f.tasks.values.to [Seq].sortWith (_.meta.asInstanceOf [HEFTDeadMetaData].rank > _.meta.asInstanceOf [HEFTDeadMetaData].rank)
                println (tasks.length)
                val ons = infra.nodes.filter (_._2.getVMs ().size != 0)
                val offs = infra.nodes.filter (_._2.getVMs ().size == 0)
                println ("NB on : " + ons.size)
                println ("NB offs : " + offs.size)
                HEFTDScheduler.scheduleBacktracks (0, 0, tasks, ons, mutex)                
            }
        }
        printNodes ()
        return allTasks (flows)
    }

    override def scheduleTasks (tasks : Seq [SchedTask], mutex : Integer) : Seq [SchedTask] = {
        println ("HDEAD does not provide by task scheduling")
        System.exit (-1)
        return tasks
    }

    def allTasks (flows : Seq [Workflow]) : Seq [SchedTask] = {
        var lst : Seq [SchedTask] = List ()
        for (x <- flows) {
            for (t <- x.tasks)
                lst = lst :+ t._2
        }
        lst
    }

    override def isSchedulable (flow : Workflow) : Boolean = {
        for (t <- flow.tasks) {
            if (t._2.loc != null)
                return false
        }
        return true
    }

    /**
      * ***************************
      * ALGO PRINCIPAL UTILS
      * ***************************
      */

    def entryMinDeadRank (flow : Workflow) : Long = {
        var res : Long = Long.MaxValue
        for (x <- flow.tasks.values.filter (_.isEntry ())) {
            val deadRank = x.meta.asInstanceOf [HEFTDeadMetaData].deadRank
            if (deadRank < res)
                res = deadRank
        }
        res
    }


    object HEFTDScheduler {

        def scheduleBacktracks (id : Int, fst : Int, tasks : Seq [SchedTask], ons : Map [String, SchedNode], mutex : Integer, direct : Boolean = false) : Boolean = {
            if (fst  >= tasks.length) return true
            val task = tasks (fst)

            if (!direct) {
                var current = System.currentTimeMillis () / 1000

                val (succ, loc) = HEFTScheduler.scheduleTask (task, ons, mutex)

                if (succ && (loc.end + current) < task.meta.asInstanceOf [HEFTDeadMetaData].submitDead) {
                    loc.apply
                    val success = scheduleBacktracks (id, fst + 1, tasks, ons, mutex)
                    if (success) return success
                } else println ("Fail : " + fst)
            }

            if (id == fst) {
                println ("FST : " + id)
                val begin = System.currentTimeMillis ()
                var vms : Map [Long, SchedVM] = Map ()
                for (i <- fst until tasks.length) {
                    if (tasks (i).loc != null) {
                        val (app_list, vm_list) = tasks (i).loc.totalReleaseFast ()
                        for (v <- vm_list) {
                            vms = vms + (v.id -> v)
                        }
                    }
                }

                for (v <- vms) {
                    v._2.validatePostRemoveFast ()
                }

                val (_, loc) = HEFTScheduler.scheduleTask (task, infra.nodes, mutex)
                loc.apply
                val ons_ = infra.nodes.filter (_._2.getVMs ().size != 0)
                if (ons_.size == ons.size) { // The backtrack will fail the same way, directly advance to the next backtrack
                    return scheduleBacktracks (id + 1, fst + 1, tasks, ons_, mutex, true)
                } else 
                      return scheduleBacktracks (id + 1, fst + 1, tasks, ons_, mutex)
            } else return false
        }

        def heftDComputeRanks (flow : Workflow, infra : Infrastructure) : Unit = {
            val entry = flow.tasks.values.filter (_.isEntry ())
            val sub = System.currentTimeMillis () / 1000
            for (t <- entry) {
                heftDComputeRanks (t, infra, sub, flow.deadline)
            }
        }


        def heftDComputeRanks (task : SchedTask, infra : Infrastructure, subTime : Long, deadline : Long) : (Long, Long) = {
            if (task.meta != null) {
                (task.meta.asInstanceOf [HEFTDeadMetaData].deadline.toLong, task.meta.asInstanceOf [HEFTDeadMetaData].rank.toLong)
            } else {
                var max : Long = 0
                var max_D : Long = deadline
                var pass : Boolean = false
                for (s <- task.work.succ) {
                    if (s.task != null) {
                        pass = true
                        val (dead, rank) = heftDComputeRanks (s.task, infra, subTime, deadline)
                        var mean_com = 0
                        for (n <- infra.nodes) for (m <- infra.nodes) {
                            val current = s.file.size / infra.bw (n._2.cluster)(m._2.cluster)
                            mean_com += current
                        }
                        mean_com /= (infra.nodes.size * infra.nodes.size)
                        if (dead - mean_com < max_D) max_D = dead - mean_com
                        if (rank + mean_com > max) max = rank
                    }
                }

                val len = task.work.computeLen (Utils.PROBA).toInt
                var mean_time = 0
                for (n <- infra.nodes)
                    mean_time += ceil ((len / (n._2.getSpeed * Utils.SPEED_DEGRAD))).toInt
                mean_time /= infra.nodes.size


                if (!pass) {
                    task.meta = new HEFTDeadMetaData (subTime, mean_time, deadline, max + mean_time)
                    (deadline, max + mean_time)
                } else {
                    task.meta = new HEFTDeadMetaData (subTime, mean_time, max_D, max + mean_time)
                    (max_D - mean_time, max + mean_time)
                }
            }
        }

    }


    object HEFTScheduler {

        def scheduleTask (task : SchedTask, nodes : Map[String, SchedNode], mutex : Integer) : (Boolean, LocTask)  = {
            var loc : LocTask = null
            for (node <- nodes) {
                mutex.synchronized {
                    val aux_loc = scheduleTaskOnNode (task, node._2)

                    if (aux_loc != null) {
                        if (loc == null) loc = aux_loc
                        else if (loc.end > aux_loc.end) loc = aux_loc
                    }
                }
            }

            if (loc != null) {
                if (loc.start < currentTime () - 20) {
		    printNodes ()
                    println (loc.start, currentTime () - 20)
                    System.out.println ("NOOOOOON")
                    System.exit (-1)
                }                    
                return (true, loc)
            } else {
                println (s"[1;31m Task sched fail : ${task.id}[0m")
                return (false, null)
            }

        }


        def scheduleTaskOnNode (task : SchedTask, node : SchedNode) : LocTask = {
            var loc : LocTask = null
            val zero = currentTime ()
            val minStart = minStartTime (task, node)

            for (v <- node.getVMs) {
                var aux_loc = v._2.getPlace (zero, minStart, task)
                if (aux_loc != null) {
                    if (aux_loc.start == -1) {
                        println ("After new vm")
                    }
                    if (loc == null) loc = aux_loc
                    else if (loc.end > aux_loc.end) loc = aux_loc
                }
            }

            val n_vm = new SchedVM (IdGenerator.nextId (), node, task.work.user, task.work.os, computeBoot (task.work.os) / node.getSpeed)

            val aux_loc = n_vm.getPlaceOnNewVM (zero, minStart, task)
            if (aux_loc != null) {
                if (aux_loc.start == -1) {
                    println ("After new vm")
                }
                if (loc == null) {
                    loc = aux_loc
                } else if (loc.end > aux_loc.end)  {
                    loc = aux_loc
                }
            }
            loc
        }

        def heftComputeRanks (flow : Workflow, infra : Infrastructure) : Unit = {
            val entry = flow.tasks.values.filter (_.isEntry ())
            for (t <- entry) {
                heftComputeRanks (t, infra)
            }
        }

        def heftComputeRanks (task : SchedTask, infra : Infrastructure) : Int = {
            if (task.meta != null) {
                task.meta.asInstanceOf [HEFTMetaData].rank
            } else {
                var max = 0
                for (s <- task.work.succ) {
                    if (s.task != null) {
                        val rank = heftComputeRanks (s.task, infra)
                        var mean_com = 0
                        for (n <- infra.nodes) for (m <- infra.nodes) {
                            mean_com += ceil((s.file.size * 10) / infra.bw (n._2.cluster)(m._2.cluster)).toInt
                        }
                        mean_com /= (infra.nodes.size * infra.nodes.size)
                        
                        if (rank + mean_com > max) max = rank + mean_com
                    }
                }

                val len = task.work.computeLen (Utils.PROBA).toInt
                var mean_time = 0
                for (n <- infra.nodes)
                    mean_time += (ceil ((len / (n._2.getSpeed * Utils.SPEED_DEGRAD))).toLong + Utils.TIME_ORDER).toInt
                mean_time /= infra.nodes.size
                mean_time += 1

                task.meta = new HEFTMetaData (max + mean_time)
                max + mean_time
            }
        }

    }
}
