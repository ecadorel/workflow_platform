package com.orch.scheduler
import akka.actor.{Props, Actor, ActorSystem, ActorRef, ActorLogging, ActorSelection, PoisonPill }
import scala.math.{max, min, ceil}
import com.orch.db._
import util.control.Breaks._
import com.orch.utils._

class GreenHEFTMetaData (var rank : Int) extends MetaData {
    override def toString () : String = {
        return "R(" + rank + ")"
    }

    override def isSchedulable (t : SchedTask) : Boolean = true
}

class GreenHEFTScheduler (parent : ScheduleActor, ref : ActorSelection, RAddr : String, RPort : Int) extends Scheduler (parent, ref, RAddr, RPort) {

    override def onNewLoad (load : List [Workflow]) : Unit = {
        this.load ++= load
        for (f <- load) {
            HEFTScheduler.heftComputeRanks (f, infra)
        }
    }

    override def scheduleTasks (tasks : Seq [SchedTask], mutex : Integer) : Seq [SchedTask] = {
        println (s"Schedule HEFT : ${tasks.size}")
        val sorted = tasks.sortWith (_.meta.asInstanceOf [GreenHEFTMetaData].rank > _.meta.asInstanceOf[GreenHEFTMetaData].rank)
        var i = 1
        var res : List [SchedTask] = List ()
        for (t <- sorted) {
            print (s"\b\b\b$i")
            if (HEFTScheduler.scheduleTask (t, mutex))
                res = res :+ t
            i += 1
        }


        println ("Schedule finished")
        printNodes ()
        res
    }
    


    object HEFTScheduler {

        def computeEnergy (node : SchedNode, loc : LocTask): Long = {
            val A = 145
            val B = 16


            val cpu = node.getCapas() ("cpus")
            val zerousage = Interval (0, loc.end, 1)
            val usage = Interval.allIntersect (node.computeUsageCPU () :+ zerousage)
            var energy = 0.0
            var energy2 = 0.0
            for (it <- usage) {
                val load =
                    if (it.height == 1) 0.1
                    else ((it.height - 1) * 1.0) / (cpu * 1.0)
                val power = A + (B * Math.log (load))
                energy = energy + power * (it.end - it.begin)
            }

            val usage2 = Interval.allIntersect (node.computeUsageCPU () :+ zerousage :+ Interval (loc.start, loc.end, loc.task.work.needs ("cpus")))

            for (it <- usage) {
                val load =
                    if (it.height == 1) 0.1
                    else ((it.height - 1) * 1.0) / (cpu * 1.0)
                val power = A + (B * Math.log (load))
                energy2 = energy2 + power * (it.end - it.begin)
            }

            (energy2 - energy).toLong
        }


        def scheduleTask (task : SchedTask, mutex : Integer) : Boolean = {
            var loc : LocTask = null
            var addedEnergy = Long.MaxValue

            for (node <- infra.nodes) {
                mutex.synchronized {
                    val aux_loc = scheduleTaskOnNode (task, node._2)

                    if (aux_loc != null) {
                        if (loc == null) loc = aux_loc
                        else {
                            val addEnergy = computeEnergy (node._2, aux_loc)
                            if (addEnergy < addedEnergy || (addedEnergy == addEnergy && loc.end > aux_loc.end)) {
                                loc = aux_loc
                                addedEnergy = addEnergy
                            }
                        }
                    }
                }
            }

            if (loc != null) {
                mutex.synchronized {
                    if (loc.start < currentTime () - 20) {
		        printNodes ()
                        println (loc.start, currentTime () - 20)
                        System.out.println ("NOOOOOON")
                        System.exit (-1)
                    }
                    
                    loc.apply ()
                }
                true
            } else {
                println (s"[1;31m Task sched fail : ${task.id}[0m")
                false
            }

        }


        def scheduleTaskOnNode (task : SchedTask, node : SchedNode) : LocTask = {
            var loc : LocTask = null
            val zero = currentTime ()
            val minStart = minStartTime (task, node)

            for (v <- node.getVMs) {
                var aux_loc = v._2.getPlace (zero, minStart, task)
                if (aux_loc != null) {
                    if (aux_loc.start == -1) {
                        println ("After new vm")
                    }
                    if (loc == null) loc = aux_loc
                    else if (loc.end > aux_loc.end) loc = aux_loc
                }
            }

            val n_vm = new SchedVM (IdGenerator.nextId (), node, task.work.user, task.work.os, computeBoot (task.work.os) / node.getSpeed)

            val aux_loc = n_vm.getPlaceOnNewVM (zero, minStart, task)
            if (aux_loc != null) {
                if (aux_loc.start == -1) {
                    println ("After new vm")
                }
                if (loc == null) {
                    loc = aux_loc
                } else if (loc.end > aux_loc.end)  {
                    loc = aux_loc
                }
            }
            loc
        }

        def heftComputeRanks (flow : Workflow, infra : Infrastructure) : Unit = {
            val entry = flow.tasks.values.filter (_.isEntry ())
            for (t <- entry) {
                heftComputeRanks (t, infra)
            }
        }

        def heftComputeRanks (task : SchedTask, infra : Infrastructure) : Int = {
            if (task.meta != null) {
                task.meta.asInstanceOf [GreenHEFTMetaData].rank
            } else {
                var max = 0
                for (s <- task.work.succ) {
                    if (s.task != null) {
                        val rank = heftComputeRanks (s.task, infra)
                        var mean_com = 0
                        for (n <- infra.nodes) for (m <- infra.nodes) {
                            mean_com += ceil((s.file.size * 10) / infra.bw (n._2.cluster)(m._2.cluster)).toInt
                        }
                        mean_com /= (infra.nodes.size * infra.nodes.size)
                        
                        if (rank + mean_com > max) max = rank + mean_com
                    }
                }

                val len = task.work.computeLen (Utils.PROBA).toInt
                var mean_time = 0
                for (n <- infra.nodes)
                    mean_time += (ceil ((len / (n._2.getSpeed * Utils.SPEED_DEGRAD))).toLong + Utils.TIME_ORDER).toInt
                mean_time /= infra.nodes.size
                mean_time += 1

                task.meta = new GreenHEFTMetaData (max + mean_time)
                max + mean_time
            }
        }

    }
}
