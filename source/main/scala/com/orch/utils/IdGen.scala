package com.orch.utils

import java.time.Instant
import java.net.NetworkInterface;
import java.security.SecureRandom;
import scala.math._

object IdGenerator {

    val TOTAL_BITS = 64
    val EPOCH_BITS = 42
    val NODE_ID_BITS = 10
    val SEQUENCE_BITS = 12

    val maxNodeId : Long = pow (2, NODE_ID_BITS).toLong - 1
    val maxSequence : Long = pow (2, SEQUENCE_BITS).toLong - 1

    val CUSTOM_EPOCH = 1420070400000L

    var lastTimeStamp : Long = -1L
    var sequence : Long = 0L

    var mutex : Integer = new Integer (0);
    var lastId : Long = 1;

    val nodeId = createNodeId ()

    def nextId () : Long = {
        // var currentTimeStamp = timestamp ()
        // if (currentTimeStamp == lastTimeStamp) {
        //     sequence = (sequence + 1) & maxSequence
        //     if (sequence == 0)
        //         currentTimeStamp = waitNextMillis (currentTimeStamp)
        // } else {
        //     sequence = 0
        // }
        
        // lastTimeStamp = currentTimeStamp
        // var id = currentTimeStamp << (TOTAL_BITS - EPOCH_BITS)
        // id |= (nodeId << (TOTAL_BITS - EPOCH_BITS - NODE_ID_BITS))
        // id |= sequence

        val id = mutex.synchronized {
            lastId += 1
            lastId
        }

        return id
    }

    def timestamp () : Long = {
        return Instant.now ().toEpochMilli () - CUSTOM_EPOCH
    }

    def waitNextMillis (currentTimeStamp : Long) : Long = {
        var curr = currentTimeStamp
        while (curr == lastTimeStamp) {
            curr = timestamp ()
        }
        return curr
    }

    def createNodeId () : Long = {
        var nodeId : Long = 0
        try {
            var sb = new StringBuilder ()
            val networkInts = NetworkInterface.getNetworkInterfaces ()
            while (networkInts.hasMoreElements ()) {
                val networkInt = networkInts.nextElement ()
                val mac = networkInt.getHardwareAddress ()
                if (mac != null) {
                    for (i <- 0 until mac.length)
                        sb.append (s"${mac (i)}%02X")
                }
            }

            nodeId = sb.toString ().hashCode ()
        } catch {
            case _ : Throwable => {
                nodeId = new SecureRandom ().nextInt ()
            }
        }

        nodeId = nodeId & maxNodeId
        return nodeId
    }

}
