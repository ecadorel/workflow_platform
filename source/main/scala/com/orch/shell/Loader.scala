package com.orch.shell

import java.io._
import java.util.{Map => JMap, List => JArray}
import org.yaml.snakeyaml.Yaml
import scala.collection.JavaConverters._
import com.orch.db._

object Loader {

    def parseNodeFile (filename : String) : Seq[Node] = {
        var res : Seq [Node] = Seq ()
        val yaml : Yaml = new Yaml
        val root = yaml.load (new FileInputStream (new java.io.File (filename)))
        val map = root.asInstanceOf[JMap[String, Object]].asScala
        val nodes = map ("computes")

        for (obj <- nodes.asInstanceOf[JArray[Object]].asScala) {
            val node = obj.asInstanceOf [JMap[String, Object]].asScala
            val name = node ("name").asInstanceOf [String]
            val addr = node ("addr").asInstanceOf [String]
            val port = node ("port").asInstanceOf [Int]
            val speed = node ("speed").asInstanceOf [Int]
            val cluster = node ("cluster").asInstanceOf [String]
            var capas : Map [String, Int] = Map ()
            for (k <- node ("capacities").asInstanceOf [JMap[String, Int]].asScala)
                capas += (k._1 -> k._2)
            res = res :+ Node (name, capas, speed, cluster, addr, port)
        }
        res
    }

    def parseClusterFile (filename : String) : Map [String, Map [String, Int]] = {
        var res : Map [String, Map [String, Int]] = Map ()
        val yaml : Yaml = new Yaml
        val root = yaml.load (new FileInputStream (new java.io.File (filename)))
        val map = root.asInstanceOf[JMap[String, Object]].asScala
        val nodes = map ("cluster")

        for (obj <- nodes.asInstanceOf[JArray[Object]].asScala) {
            val node = obj.asInstanceOf [JMap[String, Object]].asScala
            val name = node ("name").asInstanceOf [String]
            var bw : Map [String, Int] = Map () 
            for (k <- node ("bw").asInstanceOf [JMap[String, Int]].asScala)
                bw += (k._1 -> k._2)
            res += (name-> bw)
        }
        res
    }

}
